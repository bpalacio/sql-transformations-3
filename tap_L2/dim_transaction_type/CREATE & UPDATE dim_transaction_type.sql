DROP TABLE IF EXISTS tap_l2.dim_transaction_type;

CREATE TABLE  tap_l2.dim_transaction_type
AS
SELECT
	transaction_type_id as transaction_type_id 
	,UPPER(description) as transaction_type_desc
	,_fivetran_synced as _l1_audit_time
	,current_timestamp as _dim_transaction_type_audit_time
FROM tap_l1_pg_ledger_public.transaction_type;