/*
Daily INSERT fact_appsflyer_events
--No se necesita hacer upsert porque no existe modifiacación en eventos viejos para tabla L1. Sólo Insert
*/


INSERT INTO tap_l2.fact_appsflyer_events
  --Se inserta lo reciente (_fivetran_synced > _last_updated)

  --Primero se toma fecha máxima de insert (audit_l1) para traer lo nuevo.
  WITH 
    last_updated AS
      (SELECT 
          MAX(CASE WHEN device_os = 'ios' THEN _l1_audit_time ELSE NULL END) as ios_last_updated,
          MAX(CASE WHEN device_os = 'android' THEN _l1_audit_time ELSE NULL END) as android_last_updated
      FROM tap_l2.fact_appsflyer_events)

  SELECT 
        event_time::date as event_date,
        event_time::timestamp as event_time,
        install_time::timestamp as install_time,
        customer_user_id as external_ref,
    	lower(media_source) as media_source,
        lower(af_channel) as channel,
        lower(event_name) as event_name,
        lower(event_value)
        appsflyer_id,
        lower(af_adset),
        campaign,
        'ios' as device_os,
		_fivetran_synced as _l1_audit_time,
		current_timestamp as _fact_appsflyer_events_audit_time  
  FROM tap_l1_appsflyer_ios.event
      ,last_updated 
  WHERE _fivetran_synced > ios_last_updated
  UNION
  SELECT 
        event_time::date as event_date,
        event_time::timestamp as event_time,
        install_time::timestamp as install_time,
        customer_user_id as external_ref,
    	lower(media_source) as media_source,
        lower(af_channel) as channel,
        lower(event_name) as event_name,
        lower(event_value)
        appsflyer_id,
        lower(af_adset),
        campaign,
        'android' as device_os,
		_fivetran_synced as _l1_audit_time,
		current_timestamp as _fact_appsflyer_events_audit_time  
  FROM tap_l1_appsflyer_android.event
      ,last_updated 
  WHERE _fivetran_synced > android_last_updated