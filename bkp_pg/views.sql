drop view if exists bi.vw_account_contador;

drop view if exists bi.vw_account_service_charge;

drop view if exists bi.vw_transaction;

drop view if exists bi.vw_acumulados;

drop view if exists bi.vw_aux_cashins;

drop view if exists bi."vw_aux_descriptions_DELETE";

drop view if exists bi.vw_aux_transaction;

drop view if exists bi.vw_beyond_errores;

drop view if exists bi.vw_cashin_mide;

drop view if exists bi.vw_company;

drop view if exists bi.vw_full_account;

drop view if exists bi.vw_full_account_2;

drop view if exists bi."vw_full_account_OLD";

drop view if exists bi."vw_full_company_DELETE";

drop view if exists bi.vw_full_person;

drop view if exists bi.vw_loyalty_full;

drop view if exists bi.vw_prepaid_card;

drop view if exists bi.vw_prepaid_errores;

drop view if exists bi.vw_prepaid_sow;

drop view if exists bi.vw_promotion_completa;

drop view if exists bi.vw_promotion_complete;

drop view if exists bi.vw_ranking_usuarios;

drop view if exists bi.vw_recargas;

drop view if exists bi.vw_retencion_mide;

drop view if exists bi.vw_sellos_rewards;

drop view if exists bi.vw_rewards;

drop view if exists bi.vw_sellos;

drop view if exists bi.vw_transaction_beyond;

drop view if exists bi.vw_transaction_prepaid;

drop view if exists bi.vw_users;

drop view if exists bi."vw_wc_movements_details_moveid_DELETE";

drop view if exists bi.vw_wc_mov_details_resumen_id;

drop view if exists bi.vw_wc_movements;

drop view if exists bi."vw_wc_movements_DELETE";

drop view if exists bi.vw_wc_movements_dia;

drop view if exists bi."vw_wc_movements_diahora_DELETE";

drop view if exists bi."vw_wc_movements_historico_DELETE";

drop view if exists bi.vw_wc_movements_moveid;

create view bi.vw_account_contador
            (account_id, email, phone, external_ref, fecha_creacion, lifetested, name_person, last_name_person,
             dni_person, cuit_person, name_company, document_company, street, street_number, postal_address, province,
             address_person, address_company)
as
SELECT ac.account_id,
       ac.email,
       ac.phone,
       ac.account_hash AS external_ref,
       ac.created_at   AS fecha_creacion,
       ac.lifetested,
       pe.name         AS name_person,
       pe.last_name    AS last_name_person,
       pe.document_id  AS dni_person,
       pe.cuit         AS cuit_person,
       co.name         AS name_company,
       co.document     AS document_company,
       al.street,
       al.street_number,
       al.postal_address,
       pr.name         AS province,
       pe.address      AS address_person,
       co.address      AS address_company
FROM app.account ac
         LEFT JOIN app.person pe USING (account_id)
         LEFT JOIN app.company co USING (account_id)
         LEFT JOIN app.account_location al USING (account_id)
         LEFT JOIN app.province pr ON al.province_id = pr.province_id;

alter table bi.vw_account_contador
    owner to bi;

--grant select on bi.vw_account_contador to qs;

create view bi.vw_account_service_charge
            (referencia, email, telefono, account_id, device_id, external_ref, cuit, dni, transaction) as
SELECT accsc.reference_alias                                        AS referencia,
       acc.email,
       acc.phone                                                    AS telefono,
       accsc.account_id,
       acc.device_id,
       acc.account_hash                                             AS external_ref,
       ori.document                                                 AS cuit,
       COALESCE("right"("left"(ori.document, 10), 8), ori.document) AS dni,
       accsc.metadata::json ->> 'tx'::text                          AS transaction
FROM app.account_service_charge accsc
         LEFT JOIN app.account acc ON accsc.account_id = acc.account_id
         LEFT JOIN ledger.origin ori ON acc.account_hash = ori.external_ref;

alter table bi.vw_account_service_charge
    owner to bi;

--grant select on bi.vw_account_service_charge to qs;

create view bi.vw_transaction
            (transaction, transaction_id, transaction_date, transaction_dia, transaction_hour, movement_tx, movement_id,
             description_ok, amount, amount_abs, is_qr, details, origin_id, extref_origin, origin_name, origin_document,
             origin_type_ok, tx_origin_id, tx_extref_origin, tx_origin_name, tx_origin_document, tx_origin_type_ok,
             origin_balance, origin_tipo_persona, tipo_tx, fecha_creacion_cuenta, fecha_eliminacion_cuenta, target,
             target_name, target_document, target_type_ok, private, t2t_emisor_extref, t2t_emisor_nombre,
             t2t_emisor_document, mes_año, tx_activa, fecha_tx_activa)
as
SELECT CASE
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL THEN concat(m1.id, 'D-', m1.tx)
           ELSE t1.transaction_id
           END                       AS transaction,
       CASE
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL THEN concat(m1.id, '-D')
           ELSE t1.id::text
           END                       AS transaction_id,
       CASE
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND
                (lower(m1.details) ~~ '%bonifica%'::text OR lower(m1.details) ~~ '%reintegro%'::text OR
                 lower(m1.details) ~~ '%devoluc%'::text OR lower(m1.details) ~~ '%reembols%'::text)
               THEN m1.created_at - '03:00:00'::time without time zone::interval
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) !~~ '%bonifica%'::text AND
                lower(m1.details) !~~ '%reintegro%'::text AND lower(m1.details) !~~ '%devoluc%'::text AND
                lower(m1.details) !~~ '%reembols%'::text THEN m1.created_at
           ELSE t1.created_at - '03:00:00'::time without time zone::interval
           END                       AS transaction_date,
       CASE
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND
                (lower(m1.details) ~~ '%bonifica%'::text OR lower(m1.details) ~~ '%reintegro%'::text OR
                 lower(m1.details) ~~ '%devoluc%'::text OR lower(m1.details) ~~ '%reembols%'::text) THEN to_char(
                       m1.created_at - '03:00:00'::time without time zone::interval, 'YYYY-MM-DD'::text)
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) !~~ '%bonifica%'::text AND
                lower(m1.details) !~~ '%reintegro%'::text AND lower(m1.details) !~~ '%devoluc%'::text AND
                lower(m1.details) !~~ '%reembols%'::text THEN to_char(m1.created_at, 'YYYY-MM-DD'::text)
           ELSE to_char(t1.created_at - '03:00:00'::time without time zone::interval, 'YYYY-MM-DD'::text)
           END                       AS transaction_dia,
       CASE
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND
                (lower(m1.details) ~~ '%bonifica%'::text OR lower(m1.details) ~~ '%reintegro%'::text OR
                 lower(m1.details) ~~ '%devoluc%'::text OR lower(m1.details) ~~ '%reembols%'::text) THEN to_char(
                       m1.created_at - '03:00:00'::time without time zone::interval, 'HH24:MI:SS'::text)
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) !~~ '%bonifica%'::text AND
                lower(m1.details) !~~ '%reintegro%'::text AND lower(m1.details) !~~ '%devoluc%'::text AND
                lower(m1.details) !~~ '%reembols%'::text THEN to_char(m1.created_at, 'HH24:MI:SS'::text)
           ELSE to_char(t1.created_at - '03:00:00'::time without time zone::interval, 'HH24:MI:SS'::text)
           END                       AS transaction_hour,
       m1.tx                         AS movement_tx,
       m1.id                         AS movement_id,
       CASE
           WHEN tt.description = 'WORKER ACCOUNT BALANCER'::text AND lower(m1.details) ~~ '%a tu cvu%'::text
               THEN 'EXTERNAL TRANSFER'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%a tu cvu%'::text
               THEN 'EXTERNAL TRANSFER'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%ajuste%'::text
               THEN 'WORKER ACCOUNT BALANCER'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%transferencia manual%'::text
               THEN 'TRANSFER TAP TO TAP'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%te transfi%'::text
               THEN 'TRANSFER TAP TO TAP'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%fue transfe%'::text
               THEN 'TRANSFER TAP TO TAP'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND
                lower(m1.details) ~~ '%acreditado desde la tarjeta%'::text AND m1.details ~~ '%bito%'::text
               THEN 'CASH IN DEBIT CARD'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND
                lower(m1.details) ~~ '%acreditado desde la tarjeta%'::text AND m1.details !~~ '%bito%'::text
               THEN 'CASH IN CREDIT CARD'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%transferiste a%'::text
               THEN 'TRANSFERS TO CBU'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%cash in a la wc%'::text
               THEN 'WC CASH IN'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%la cuenta wc%'::text AND
                lower(m1.details) ~~ '%fondeo%'::text THEN 'WC_FOUND'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%worker concilia%'::text
               THEN 'WORKER ACCOUNT BALANCER'::text
           WHEN lower(m1.details) ~~ '%bonifica%'::text OR lower(m1.details) ~~ '%reintegro%'::text OR
                lower(m1.details) ~~ '%devoluc%'::text OR lower(m1.details) ~~ '%reembols%'::text
               THEN 'BONIF/REINTEGROS'::text
           ELSE tt.description
           END                       AS description_ok,
       m1.amount,
       abs(m1.amount)                AS amount_abs,
       m1.metadata ->> 'is_qr'::text AS is_qr,
       m1.details,
       m1.origin_id,
       CASE
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL THEN m1.target
           ELSE o1.external_ref
           END                       AS extref_origin,
       CASE
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL THEN o2.name
           ELSE o1.name
           END                       AS origin_name,
       CASE
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL THEN o2.document
           ELSE o1.document
           END                       AS origin_document,
       CASE
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL THEN o2.external_ref_type
           ELSE o1.external_ref_type
           END                       AS origin_type_ok,
       t1.origin_id                  AS tx_origin_id,
       CASE
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL THEN m1.target
           ELSE o4.external_ref
           END                       AS tx_extref_origin,
       CASE
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL THEN o2.name
           ELSE o4.name
           END                       AS tx_origin_name,
       CASE
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL THEN o2.document
           ELSE o4.document
           END                       AS tx_origin_document,
       CASE
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL THEN o2.external_ref_type
           ELSE o4.external_ref_type
           END                       AS tx_origin_type_ok,
       CASE
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL THEN o2.balance
           ELSE o1.balance
           END                       AS origin_balance,
       CASE
           WHEN "left"(o1.document, 2) = '20'::text OR "left"(o1.document, 2) = '23'::text OR
                "left"(o1.document, 2) = '24'::text OR "left"(o1.document, 2) = '27'::text THEN 'Persona física'::text
           WHEN "left"(o1.document, 2) = '30'::text OR "left"(o1.document, 2) = '33'::text OR
                "left"(o1.document, 2) = '34'::text THEN 'Persona jurídica'::text
           ELSE NULL::text
           END                       AS origin_tipo_persona,
       m1.origin_type                AS tipo_tx,
       CASE
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL THEN o2.created_at
           ELSE o1.created_at
           END                       AS fecha_creacion_cuenta,
       CASE
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL THEN o2.deleted_at
           ELSE o1.deleted_at
           END                       AS fecha_eliminacion_cuenta,
       m1.target,
       o2.name                       AS target_name,
       o2.document                   AS target_document,
       o2.external_ref_type          AS target_type_ok,
       m1.private,
       CASE
           WHEN tt.description = 'TRANSFER TAP TO TAP'::text AND m1.amount > 0::numeric THEN replace(
                   "substring"(m1.details, "position"(m1.details, '{'::text) + 1,
                               "position"(m1.details, '}'::text) - "position"(m1.details, '{'::text)), '}'::text,
                   ''::text)
           ELSE NULL::text
           END                       AS t2t_emisor_extref,
       o3.name                       AS t2t_emisor_nombre,
       o3.document                   AS t2t_emisor_document,
       CASE
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND
                (lower(m1.details) ~~ '%bonifica%'::text OR lower(m1.details) ~~ '%reintegro%'::text OR
                 lower(m1.details) ~~ '%devoluc%'::text OR lower(m1.details) ~~ '%reembols%'::text) THEN concat(to_char(
                                                                                                                                (date_part(
                                                                                                                                         'year'::text,
                                                                                                                                         (m1.created_at - '03:00:00'::time without time zone::interval)::date) -
                                                                                                                                 date_part('year'::text, '2020-02-01'::date)) *
                                                                                                                                12::double precision +
                                                                                                                                (date_part(
                                                                                                                                         'month'::text,
                                                                                                                                         (m1.created_at - '03:00:00'::time without time zone::interval)::date) -
                                                                                                                                 date_part('month'::text, '2020-02-01'::date)),
                                                                                                                                '00'::text),
                                                                                                                '. ',
                                                                                                                to_char(
                                                                                                                            m1.created_at -
                                                                                                                            '03:00:00'::time without time zone::interval,
                                                                                                                            'monthYYYY'::text))
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) !~~ '%bonifica%'::text AND
                lower(m1.details) !~~ '%reintegro%'::text AND lower(m1.details) !~~ '%devoluc%'::text AND
                lower(m1.details) !~~ '%reembols%'::text THEN concat(to_char(
                                                                                     (date_part('year'::text, m1.created_at::date) -
                                                                                      date_part('year'::text, '2020-02-01'::date)) *
                                                                                     12::double precision +
                                                                                     (date_part('month'::text, m1.created_at::date) -
                                                                                      date_part('month'::text, '2020-02-01'::date)),
                                                                                     '00'::text), '. ',
                                                                     to_char(m1.created_at, 'monthYYYY'::text))
           WHEN m1.id IS NULL AND t1.transaction_id IS NULL THEN NULL::text
           ELSE concat(to_char((date_part('year'::text,
                                          (t1.created_at - '03:00:00'::time without time zone::interval)::date) -
                                date_part('year'::text, '2020-02-01'::date)) * 12::double precision +
                               (date_part('month'::text,
                                          (t1.created_at - '03:00:00'::time without time zone::interval)::date) -
                                date_part('month'::text, '2020-02-01'::date)), '00'::text), '. ',
                       to_char(t1.created_at - '03:00:00'::time without time zone::interval, 'monthYYYY'::text))
           END                       AS "mes_año",
       CASE
           WHEN tt.description ~~* '%payment%'::text OR
                tt.description = 'TRANSFER TAP TO TAP'::text AND (m1.metadata ->> 'is_qr'::text) = 'true'::text AND
                abs(m1.amount) >= 50::numeric AND o2.external_ref_type = 'company'::text OR
                tt.description = 'TRANSFER TAP TO TAP'::text AND m1.amount < 0::numeric AND
                abs(m1.amount) >= 50::numeric AND o2.external_ref_type <> 'company'::text AND
                CASE
                    WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL THEN o2.name
                    ELSE o1.name
                    END <> 'TAP BILLETERA SA'::text THEN
               CASE
                   WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL THEN concat(m1.id, 'D-', m1.tx)
                   ELSE t1.transaction_id
                   END
           ELSE NULL::text
           END                       AS tx_activa,
       CASE
           WHEN tt.description ~~* '%payment%'::text OR
                tt.description = 'TRANSFER TAP TO TAP'::text AND (m1.metadata ->> 'is_qr'::text) = 'true'::text AND
                abs(m1.amount) >= 50::numeric AND o2.external_ref_type = 'company'::text OR
                tt.description = 'TRANSFER TAP TO TAP'::text AND m1.amount < 0::numeric AND
                abs(m1.amount) >= 50::numeric AND o2.external_ref_type <> 'company'::text AND
                CASE
                    WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL THEN o2.name
                    ELSE o1.name
                    END <> 'TAP BILLETERA SA'::text THEN
               CASE
                   WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND
                        (lower(m1.details) ~~ '%bonifica%'::text OR lower(m1.details) ~~ '%reintegro%'::text OR
                         lower(m1.details) ~~ '%devoluc%'::text OR lower(m1.details) ~~ '%reembols%'::text)
                       THEN m1.created_at - '03:00:00'::time without time zone::interval
                   WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) !~~ '%bonifica%'::text AND
                        lower(m1.details) !~~ '%reintegro%'::text AND lower(m1.details) !~~ '%devoluc%'::text AND
                        lower(m1.details) !~~ '%reembols%'::text THEN m1.created_at
                   ELSE t1.created_at - '03:00:00'::time without time zone::interval
                   END
           ELSE NULL::timestamp without time zone
           END                       AS fecha_tx_activa
FROM ledger.transaction t1
         FULL JOIN ledger.movement m1 ON t1.movement_id = m1.id
         LEFT JOIN ledger.transaction_type tt ON t1.transaction_type_id = tt.transaction_type_id
         FULL JOIN ledger.origin o1 ON o1.origin_id::text = m1.origin_id
         FULL JOIN ledger.origin o2 ON m1.target = o2.external_ref
         LEFT JOIN ledger.origin o4 ON t1.origin_id = o4.origin_id
         LEFT JOIN ledger.origin o3 ON
        CASE
            WHEN tt.description = 'TRANSFER TAP TO TAP'::text AND m1.amount > 0::numeric THEN replace(
                    "substring"(m1.details, "position"(m1.details, '{'::text) + 1,
                                "position"(m1.details, '}'::text) - "position"(m1.details, '{'::text)), '}'::text,
                    ''::text)
            ELSE ''::text
            END = o3.external_ref;

alter table bi.vw_transaction
    owner to bi;

--grant select on bi.vw_transaction to qs;

create view bi.vw_acumulados
            (extref_origin, acumulado_mide, acumulado_edenor, acumulado_aysa, acumulado_metrogas, fecha_ult_tx_mide,
             fecha_ult_tx_edenor, fecha_ult_tx_metrogas, fecha_ult_tx_aysa)
as
SELECT vw_transaction.extref_origin,
       count(DISTINCT
             CASE
                 WHEN vw_transaction.description_ok = 'LIGHTNING_PAYMENT'::text THEN vw_transaction.transaction
                 ELSE NULL::text
                 END)   AS acumulado_mide,
       count(DISTINCT
             CASE
                 WHEN vw_transaction.description_ok ~~ '%PAYMENT%'::text AND
                      vw_transaction.description_ok <> 'LIGHTNING_PAYMENT'::text AND vw_transaction.target = 'EDENOR'::text
                     THEN vw_transaction.transaction
                 ELSE NULL::text
                 END)   AS acumulado_edenor,
       count(DISTINCT
             CASE
                 WHEN vw_transaction.description_ok ~~ '%PAYMENT%'::text AND
                      vw_transaction.description_ok <> 'LIGHTNING_PAYMENT'::text AND vw_transaction.target = 'AYSA'::text
                     THEN vw_transaction.transaction
                 ELSE NULL::text
                 END)   AS acumulado_aysa,
       count(DISTINCT
             CASE
                 WHEN vw_transaction.description_ok ~~ '%PAYMENT%'::text AND
                      vw_transaction.description_ok <> 'LIGHTNING_PAYMENT'::text AND vw_transaction.target = 'METROGAS'::text
                     THEN vw_transaction.transaction
                 ELSE NULL::text
                 END)   AS acumulado_metrogas,
       max(
               CASE
                   WHEN vw_transaction.description_ok = 'LIGHTNING_PAYMENT'::text THEN vw_transaction.transaction_date::date
                   ELSE NULL::date
                   END) AS fecha_ult_tx_mide,
       max(
               CASE
                   WHEN vw_transaction.description_ok ~~ '%PAYMENT%'::text AND
                        vw_transaction.description_ok <> 'LIGHTNING_PAYMENT'::text AND vw_transaction.target = 'EDENOR'::text
                       THEN vw_transaction.transaction_date::date
                   ELSE NULL::date
                   END) AS fecha_ult_tx_edenor,
       max(
               CASE
                   WHEN vw_transaction.description_ok ~~ '%PAYMENT%'::text AND
                        vw_transaction.description_ok <> 'LIGHTNING_PAYMENT'::text AND
                        vw_transaction.target = 'METROGAS'::text THEN vw_transaction.transaction_date::date
                   ELSE NULL::date
                   END) AS fecha_ult_tx_metrogas,
       max(
               CASE
                   WHEN vw_transaction.description_ok ~~ '%PAYMENT%'::text AND
                        vw_transaction.description_ok <> 'LIGHTNING_PAYMENT'::text AND vw_transaction.target = 'AYSA'::text
                       THEN vw_transaction.transaction_date::date
                   ELSE NULL::date
                   END) AS fecha_ult_tx_aysa
FROM bi.vw_transaction
WHERE vw_transaction.extref_origin <> 'tap-wc'::text
GROUP BY vw_transaction.extref_origin;

alter table bi.vw_acumulados
    owner to bi;

--grant select on bi.vw_acumulados to qs;

create view bi.vw_aux_cashins(movement, description_aux) as
SELECT t.movement,
       t.description_aux
FROM (SELECT DISTINCT tr.movement_id AS movement,
                      CASE
                          WHEN tr.transaction_type_id = 22 AND tr2.transaction_type_id = 2
                              THEN 'CASH IN TC - PAGO SERVICIOS'::text
                          WHEN tr.transaction_type_id = 22 AND tr2.transaction_type_id = 14
                              THEN 'CASH IN TD - PAGO SERVICIOS'::text
                          WHEN tr.transaction_type_id = 14 AND tr2.transaction_type_id = 21
                              THEN 'CASH IN TD - RECARGA SERVICIOS (sin MIDE)'::text
                          WHEN tr.transaction_type_id = 21 AND tr2.transaction_type_id = 2
                              THEN 'CASH IN TC - RECARGA SERVICIOS (sin MIDE)'::text
                          WHEN tr.transaction_type_id = 14 AND tr2.transaction_type_id = 29
                              THEN 'CASH IN TD - RECARGA MIDE'::text
                          WHEN tr.transaction_type_id = 29 AND tr2.transaction_type_id = 2
                              THEN 'CASH IN TC - RECARGA MIDE'::text
                          WHEN tr.transaction_type_id = 14 AND tr2.transaction_type_id = 23
                              THEN 'CASH IN TD - TRANSFER TO CBU'::text
                          WHEN tr.transaction_type_id = 23 AND tr2.transaction_type_id = 2
                              THEN 'CASH IN TC - TRANSFER TO CBU'::text
                          WHEN tr.transaction_type_id = 14 AND tr2.transaction_type_id = 5
                              THEN 'CASH IN TD - TAP TO TAP'::text
                          WHEN tr.transaction_type_id = 5 AND tr2.transaction_type_id = 2 THEN 'CASH IN TC - TAP TO TAP'::text
                          WHEN tr.transaction_type_id = 14 AND tr2.transaction_type_id = 24
                              THEN 'CASH IN TD - RECARGA SUBE'::text
                          WHEN tr.transaction_type_id = 24 AND tr2.transaction_type_id = 2
                              THEN 'CASH IN TC - RECARGA SUBE'::text
                          ELSE NULL::text
                          END        AS description_aux
      FROM ledger.transaction tr,
           ledger.transaction tr2,
           ledger.transaction_type tt
      WHERE tr.transaction_type_id = tt.transaction_type_id
        AND tr.transaction_id = tr2.transaction_id
        AND tr.transaction_type_id <> tr2.transaction_type_id) t
WHERE t.description_aux IS NOT NULL;

alter table bi.vw_aux_cashins
    owner to bi;

--grant select on bi.vw_aux_cashins to qs;

create view bi."vw_aux_descriptions_DELETE"
            (transaction_date, transaction, transaction_id, transaction_type_id, description, description_ok,
             description_auxiliar) as
SELECT tr.created_at     AS transaction_date,
       tr.transaction_id AS transaction,
       CASE
           WHEN m1.id IS NOT NULL AND tr.transaction_id IS NULL THEN concat(m1.id, '-D')
           ELSE tr.id::text
           END           AS transaction_id,
       tr.transaction_type_id,
       tt.description,
       CASE
           WHEN tt.description = 'WORKER ACCOUNT BALANCER'::text AND lower(m1.details) ~~ '%a tu cvu%'::text
               THEN 'EXTERNAL TRANSFER'::text
           WHEN m1.id IS NOT NULL AND tr.transaction_id IS NULL AND lower(m1.details) ~~ '%a tu cvu%'::text
               THEN 'EXTERNAL TRANSFER'::text
           WHEN m1.id IS NOT NULL AND tr.transaction_id IS NULL AND lower(m1.details) ~~ '%ajuste%'::text
               THEN 'WORKER ACCOUNT BALANCER'::text
           WHEN m1.id IS NOT NULL AND tr.transaction_id IS NULL AND lower(m1.details) ~~ '%transferencia manual%'::text
               THEN 'TRANSFER TAP TO TAP'::text
           WHEN m1.id IS NOT NULL AND tr.transaction_id IS NULL AND lower(m1.details) ~~ '%te transfi%'::text
               THEN 'TRANSFER TAP TO TAP'::text
           WHEN m1.id IS NOT NULL AND tr.transaction_id IS NULL AND lower(m1.details) ~~ '%fue transfe%'::text
               THEN 'TRANSFER TAP TO TAP'::text
           WHEN m1.id IS NOT NULL AND tr.transaction_id IS NULL AND
                lower(m1.details) ~~ '%acreditado desde la tarjeta%'::text AND m1.details ~~ '%bito%'::text
               THEN 'CASH IN DEBIT CARD'::text
           WHEN m1.id IS NOT NULL AND tr.transaction_id IS NULL AND
                lower(m1.details) ~~ '%acreditado desde la tarjeta%'::text AND m1.details !~~ '%bito%'::text
               THEN 'CASH IN CREDIT CARD'::text
           WHEN m1.id IS NOT NULL AND tr.transaction_id IS NULL AND lower(m1.details) ~~ '%transferiste a%'::text
               THEN 'TRANSFERS TO CBU'::text
           WHEN m1.id IS NOT NULL AND tr.transaction_id IS NULL AND lower(m1.details) ~~ '%cash in a la wc%'::text
               THEN 'WC CASH IN'::text
           WHEN m1.id IS NOT NULL AND tr.transaction_id IS NULL AND lower(m1.details) ~~ '%la cuenta wc%'::text AND
                lower(m1.details) ~~ '%fondeo%'::text THEN 'WC_FOUND'::text
           WHEN m1.id IS NOT NULL AND tr.transaction_id IS NULL AND lower(m1.details) ~~ '%worker concilia%'::text
               THEN 'WORKER ACCOUNT BALANCER'::text
           WHEN lower(m1.details) ~~ '%bonifica%'::text OR lower(m1.details) ~~ '%reintegro%'::text OR
                lower(m1.details) ~~ '%devoluc%'::text OR lower(m1.details) ~~ '%reembols%'::text
               THEN 'BONIF/REINTEGROS'::text
           ELSE tt.description
           END           AS description_ok,
       CASE
           WHEN (EXISTS(SELECT tr_1.transaction_id
                        FROM ledger.transaction tr_1
                        WHERE tr.transaction_id = tr_1.transaction_id
                          AND tr_1.transaction_type_id = 22)) AND
                (tr.transaction_type_id = 2 OR tr.transaction_type_id = 14) THEN 'CASH IN PAGO SERVICIOS'::text
           WHEN (EXISTS(SELECT tr_1.transaction_id
                        FROM ledger.transaction tr_1
                        WHERE tr.transaction_id = tr_1.transaction_id
                          AND tr_1.transaction_type_id = 21)) AND
                (tr.transaction_type_id = 2 OR tr.transaction_type_id = 14) THEN 'CASH IN RECARGA SERVICIOS (sin MIDE)'::text
           WHEN (EXISTS(SELECT tr_1.transaction_id
                        FROM ledger.transaction tr_1
                        WHERE tr.transaction_id = tr_1.transaction_id
                          AND tr_1.transaction_type_id = 29)) AND
                (tr.transaction_type_id = 2 OR tr.transaction_type_id = 14) THEN 'CASH IN RECARGA MIDE'::text
           WHEN (EXISTS(SELECT tr_1.transaction_id
                        FROM ledger.transaction tr_1
                        WHERE tr.transaction_id = tr_1.transaction_id
                          AND tr_1.transaction_type_id = 23)) AND
                (tr.transaction_type_id = 2 OR tr.transaction_type_id = 14) THEN 'CASH IN TRANSFER TO CBU'::text
           WHEN (EXISTS(SELECT tr_1.transaction_id
                        FROM ledger.transaction tr_1
                        WHERE tr.transaction_id = tr_1.transaction_id
                          AND tr_1.transaction_type_id = 5)) AND
                (tr.transaction_type_id = 2 OR tr.transaction_type_id = 14) THEN 'CASH IN TAP TO TAP'::text
           WHEN (EXISTS(SELECT tr_1.transaction_id
                        FROM ledger.transaction tr_1
                        WHERE tr.transaction_id = tr_1.transaction_id
                          AND tr_1.transaction_type_id = 24)) AND
                (tr.transaction_type_id = 2 OR tr.transaction_type_id = 14) THEN 'CASH IN RECARGA SUBE'::text
           WHEN tr.transaction_type_id = 2 OR tr.transaction_type_id = 14 THEN 'CASH IN NORMAL'::text
           ELSE NULL::text
           END           AS description_auxiliar
FROM ledger.transaction tr
         LEFT JOIN ledger.transaction_type tt ON tr.transaction_type_id = tt.transaction_type_id
         FULL JOIN ledger.movement m1 ON tr.movement_id = m1.id;

alter table bi."vw_aux_descriptions_DELETE"
    owner to bi;

--grant select on bi."vw_aux_descriptions_DELETE" to qs;

create view bi.vw_aux_transaction(transaction_id, tipo_pago_recarga) as
SELECT t.transaction_id,
       t.tipo_pago_recarga
FROM (SELECT DISTINCT tr.transaction_id,
                      CASE
                          WHEN tr.transaction_type_id = 2 AND tr2.transaction_type_id = 22
                              THEN 'PAGO SERVICIO - TARJ CREDITO'::text
                          WHEN tr.transaction_type_id = 22 AND tr2.transaction_type_id = 14
                              THEN 'PAGO SERVICIO - TARJ DEBITO'::text
                          WHEN tr.transaction_type_id = 1 OR tr2.transaction_type_id = 1
                              THEN 'PAGO SERVICIO - DINERO EN CUENTA'::text
                          WHEN tr.transaction_type_id = 21 AND tr2.transaction_type_id = 2
                              THEN 'RECARGA SERVICIO - TARJ CREDITO'::text
                          WHEN tr.transaction_type_id = 14 AND tr2.transaction_type_id = 21
                              THEN 'RECARGA SERVICIO - TARJ DEBITO'::text
                          WHEN tr.transaction_type_id = 20 OR tr2.transaction_type_id = 20
                              THEN 'RECARGA SERVICIO - DINERO EN CUENTA'::text
                          WHEN tr.transaction_type_id = 2 AND tr2.transaction_type_id = 29
                              THEN 'RECARGA MIDE - TARJ CREDITO'::text
                          WHEN tr.transaction_type_id = 14 AND tr2.transaction_type_id = 29
                              THEN 'RECARGA MIDE - TARJ DEBITO'::text
                          ELSE NULL::text
                          END AS tipo_pago_recarga
      FROM ledger.transaction tr,
           ledger.transaction tr2,
           ledger.transaction_type tt
      WHERE tr.transaction_type_id = tt.transaction_type_id
        AND tr.transaction_id = tr2.transaction_id
        AND tr.transaction_type_id <> tr2.transaction_type_id) t
WHERE t.tipo_pago_recarga IS NOT NULL;

alter table bi.vw_aux_transaction
    owner to bi;

--grant select on bi.vw_aux_transaction to qs;

create view bi.vw_beyond_errores
            (transaction_date, tx, external_ref, error_code, error_name, error_message, status, payment_id,
             transaction_id) as
SELECT o1.created_at                   AS transaction_date,
       o1.tx,
       o1.external_ref,
       o1.response ->> 'code'::text    AS error_code,
       o1.response ->> 'name'::text    AS error_name,
       o1.response ->> 'message'::text AS error_message,
       o1.status,
       p1.payment_id,
       p1.transaction_id
FROM beyond.operations o1
         LEFT JOIN beyond.payments p1 USING (operation_id);

alter table bi.vw_beyond_errores
    owner to bi;

--grant select on bi.vw_beyond_errores to qs;

create view bi.vw_cashin_mide
            (transaction, transaction_date, transaction_dia, transaction_hour, description_ok, amount, details,
             origin_id, extref_origin)
as
SELECT vw_transaction.transaction,
       vw_transaction.transaction_date,
       vw_transaction.transaction_dia,
       vw_transaction.transaction_hour,
       vw_transaction.description_ok,
       vw_transaction.amount,
       vw_transaction.details,
       vw_transaction.origin_id,
       vw_transaction.extref_origin
FROM bi.vw_transaction
WHERE (vw_transaction.extref_origin IN (SELECT vw_transaction_1.extref_origin
                                        FROM bi.vw_transaction vw_transaction_1
                                        WHERE vw_transaction_1.description_ok ~~ '%LIGHTNING%'::text))
  AND vw_transaction.extref_origin <> 'tap-wc'::text
  AND vw_transaction.amount > 0::numeric;

alter table bi.vw_cashin_mide
    owner to bi;

--grant select on bi.vw_cashin_mide to qs;

create view bi.vw_company (cuit, business_sector_id, name, business_name, business_sector, direccion, cvu) as
SELECT a.document      AS cuit,
       a.business_sector_id,
       a.name,
       a.business_name,
       b.name          AS business_sector,
       a.address       AS direccion,
       fi.internal_ref AS cvu
FROM app.company a
         LEFT JOIN app.business_sector b ON a.business_sector_id = b.business_sector_id
         LEFT JOIN app.account ac ON a.account_id = ac.account_id
         LEFT JOIN fiat.origin fi ON ac.account_hash = fi.external_ref;

alter table bi.vw_company
    owner to bi;

--grant select on bi.vw_company to qs;

create view bi.vw_full_account
            (id, username, email, phone, device_id, external_ref, version_app, version_os, prueba_vida, nombre_person,
             apellido_person, nombre_completo_person, documento_person, fecha_nacimiento_person, cuit_person,
             persona_expuesta, sujeto_obligado, direccion_completa_person, codpostal_real_person, latitud_person,
             longitud_person, codpostal_person, localidad_person, departamento_person, provincia_person,
             nombre_compañía, nombre_dueño, persona_expuesta_company, documento_compañía, business_sector,
             direccion_completa_company, latitud_company, longitud_company, direccion_company, codpostal_company,
             localidad_company, departamento_company, provincia_company)
as
SELECT ac.account_id                                                             AS id,
       ac.username,
       ac.email,
       ac.phone,
       ac.device_id,
       ac.account_hash                                                           AS external_ref,
       ac.current_version                                                        AS version_app,
       ac.current_os                                                             AS version_os,
       ac.lifetested                                                             AS prueba_vida,
       pe.name                                                                   AS nombre_person,
       pe.last_name                                                              AS apellido_person,
       concat(pe.name, ' ', pe.last_name)                                        AS nombre_completo_person,
       pe.document_id                                                            AS documento_person,
       pe.birthdate                                                              AS fecha_nacimiento_person,
       pe.cuit                                                                   AS cuit_person,
       pe.is_exposed_person                                                      AS persona_expuesta,
       pe.is_uif_person                                                          AS sujeto_obligado,
       pe.address                                                                AS direccion_completa_person,
       CASE
           WHEN pe.address !~~ '%undefined%'::text AND "right"(pe.address, 6) ~~ '%,%'::text THEN "right"(pe.address, 4)
           ELSE '-'::text
           END                                                                   AS codpostal_real_person,
       COALESCE((((pe.location #>> '{}'::text[])::jsonb) -> 'location'::text) ->> 'lat'::text,
                ((pe.location #>> '{}'::text[])::jsonb) ->> 'lat'::text)         AS latitud_person,
       COALESCE((((pe.location #>> '{}'::text[])::jsonb) -> 'location'::text) ->> 'lng'::text,
                ((pe.location #>> '{}'::text[])::jsonb) ->> 'lng'::text)         AS longitud_person,
       ((pe.location #>> '{}'::text[])::jsonb) ->> 'postal_address'::text        AS codpostal_person,
       ((pe.location #>> '{}'::text[])::jsonb) ->> 'locality'::text              AS localidad_person,
       COALESCE(((pe.location #>> '{}'::text[])::jsonb) ->> 'department'::text,
                ((pe.location #>> '{}'::text[])::jsonb) ->> 'deparment'::text)   AS departamento_person,
       initcap(replace(replace(replace(((pe.location #>> '{}'::text[])::jsonb) ->> 'province'::text, ' Province'::text,
                                       ''::text), 'Provincia de Buenos Aires'::text, 'Buenos Aires'::text),
                       ' Department'::text, ''::text))                           AS provincia_person,
       co.name                                                                   AS "nombre_compañía",
       co.business_name                                                          AS "nombre_dueño",
       co.is_exposed_person                                                      AS persona_expuesta_company,
       co.document                                                               AS "documento_compañía",
       bs.name                                                                   AS business_sector,
       co.address                                                                AS direccion_completa_company,
       COALESCE((((co.location #>> '{}'::text[])::jsonb) -> 'location'::text) ->> 'lat'::text,
                ((co.location #>> '{}'::text[])::jsonb) ->> 'lat'::text)         AS latitud_company,
       COALESCE((((co.location #>> '{}'::text[])::jsonb) -> 'location'::text) ->> 'lng'::text,
                ((co.location #>> '{}'::text[])::jsonb) ->> 'lng'::text)         AS longitud_company,
       concat(((co.location #>> '{}'::text[])::jsonb) ->> 'street'::text, ' ',
              ((co.location #>> '{}'::text[])::jsonb) ->> 'street_number'::text) AS direccion_company,
       ((co.location #>> '{}'::text[])::jsonb) ->> 'postal_address'::text        AS codpostal_company,
       ((co.location #>> '{}'::text[])::jsonb) ->> 'locality'::text              AS localidad_company,
       COALESCE(((co.location #>> '{}'::text[])::jsonb) ->> 'department'::text,
                ((co.location #>> '{}'::text[])::jsonb) ->> 'deparment'::text)   AS departamento_company,
       initcap(replace(replace(replace(((co.location #>> '{}'::text[])::jsonb) ->> 'province'::text, ' Province'::text,
                                       ''::text), 'Provincia de Buenos Aires'::text, 'Buenos Aires'::text),
                       ' Department'::text, ''::text))                           AS provincia_company
FROM app.account ac
         LEFT JOIN app.company co ON ac.account_id = co.account_id
         LEFT JOIN app.person pe ON ac.account_id = pe.account_id
         LEFT JOIN app.business_sector bs ON co.business_sector_id = bs.business_sector_id;

alter table bi.vw_full_account
    owner to bi;

--grant select on bi.vw_full_account to qs;

create view bi.vw_full_account_2
            (id, username, email, phone, device_id, external_ref, version_app, version_os, prueba_vida, nombre_person,
             apellido_person, nombre_completo_person, documento_person, fecha_nacimiento_person, cuit_person,
             persona_expuesta, sujeto_obligado, direccion_completa_person, codpostal_real_person, latitud_person,
             longitud_person, codpostal_person, localidad_person, departamento_person, provincia_person,
             nombre_compañía, nombre_dueño, persona_expuesta_company, documento_compañía, business_sector,
             direccion_completa_company, latitud_company, longitud_company, direccion_company, codpostal_company,
             localidad_company, departamento_company, provincia_company, sujeto_expuesto)
as
SELECT ac.account_id                                                             AS id,
       ac.username,
       ac.email,
       ac.phone,
       ac.device_id,
       ac.account_hash                                                           AS external_ref,
       ac.current_version                                                        AS version_app,
       ac.current_os                                                             AS version_os,
       ac.lifetested                                                             AS prueba_vida,
       pe.name                                                                   AS nombre_person,
       pe.last_name                                                              AS apellido_person,
       concat(pe.name, ' ', pe.last_name)                                        AS nombre_completo_person,
       pe.document_id                                                            AS documento_person,
       pe.birthdate                                                              AS fecha_nacimiento_person,
       pe.cuit                                                                   AS cuit_person,
       pe.is_exposed_person                                                      AS persona_expuesta,
       pe.is_uif_person                                                          AS sujeto_obligado,
       pe.address                                                                AS direccion_completa_person,
       CASE
           WHEN pe.address !~~ '%undefined%'::text AND "right"(pe.address, 6) ~~ '%,%'::text THEN "right"(pe.address, 4)
           ELSE '-'::text
           END                                                                   AS codpostal_real_person,
       COALESCE((((pe.location #>> '{}'::text[])::jsonb) -> 'location'::text) ->> 'lat'::text,
                ((pe.location #>> '{}'::text[])::jsonb) ->> 'lat'::text)         AS latitud_person,
       COALESCE((((pe.location #>> '{}'::text[])::jsonb) -> 'location'::text) ->> 'lng'::text,
                ((pe.location #>> '{}'::text[])::jsonb) ->> 'lng'::text)         AS longitud_person,
       ((pe.location #>> '{}'::text[])::jsonb) ->> 'postal_address'::text        AS codpostal_person,
       ((pe.location #>> '{}'::text[])::jsonb) ->> 'locality'::text              AS localidad_person,
       COALESCE(((pe.location #>> '{}'::text[])::jsonb) ->> 'department'::text,
                ((pe.location #>> '{}'::text[])::jsonb) ->> 'deparment'::text)   AS departamento_person,
       initcap(replace(replace(replace(((pe.location #>> '{}'::text[])::jsonb) ->> 'province'::text, ' Province'::text,
                                       ''::text), 'Provincia de Buenos Aires'::text, 'Buenos Aires'::text),
                       ' Department'::text, ''::text))                           AS provincia_person,
       co.name                                                                   AS "nombre_compañía",
       co.business_name                                                          AS "nombre_dueño",
       co.is_exposed_person                                                      AS persona_expuesta_company,
       co.document                                                               AS "documento_compañía",
       bs.name                                                                   AS business_sector,
       co.address                                                                AS direccion_completa_company,
       COALESCE((((co.location #>> '{}'::text[])::jsonb) -> 'location'::text) ->> 'lat'::text,
                ((co.location #>> '{}'::text[])::jsonb) ->> 'lat'::text)         AS latitud_company,
       COALESCE((((co.location #>> '{}'::text[])::jsonb) -> 'location'::text) ->> 'lng'::text,
                ((co.location #>> '{}'::text[])::jsonb) ->> 'lng'::text)         AS longitud_company,
       concat(((co.location #>> '{}'::text[])::jsonb) ->> 'street'::text, ' ',
              ((co.location #>> '{}'::text[])::jsonb) ->> 'street_number'::text) AS direccion_company,
       ((co.location #>> '{}'::text[])::jsonb) ->> 'postal_address'::text        AS codpostal_company,
       ((co.location #>> '{}'::text[])::jsonb) ->> 'locality'::text              AS localidad_company,
       COALESCE(((co.location #>> '{}'::text[])::jsonb) ->> 'department'::text,
                ((co.location #>> '{}'::text[])::jsonb) ->> 'deparment'::text)   AS departamento_company,
       initcap(replace(replace(replace(((co.location #>> '{}'::text[])::jsonb) ->> 'province'::text, ' Province'::text,
                                       ''::text), 'Provincia de Buenos Aires'::text, 'Buenos Aires'::text),
                       ' Department'::text, ''::text))                           AS provincia_company,
       pe.is_uif_person                                                          AS sujeto_expuesto
FROM app.account ac
         LEFT JOIN app.company co ON ac.account_id = co.account_id
         LEFT JOIN app.person pe ON ac.account_id = pe.account_id
         LEFT JOIN app.business_sector bs ON co.business_sector_id = bs.business_sector_id;

alter table bi.vw_full_account_2
    owner to bi;

--grant select on bi.vw_full_account_2 to qs;

create view bi."vw_full_account_OLD"
            (id, username, email, phone, device_id, external_ref, version_app, version_os, prueba_vida, nombre_person,
             apellido_person, nombre_completo_person, documento_person, fecha_nacimiento_person, cuit_person,
             persona_expuesta, direccion_completa_person, codpostal_real_person, latitud_person, longitud_person,
             codpostal_person, localidad_person, departamento_person, provincia_person, nombre_compañía, nombre_dueño,
             documento_compañía, business_sector, direccion_completa_company, latitud_company, longitud_company,
             direccion_company, codpostal_company, localidad_company, departamento_company, provincia_company)
as
SELECT ac.account_id                                                             AS id,
       ac.username,
       ac.email,
       ac.phone,
       ac.device_id,
       ac.account_hash                                                           AS external_ref,
       ac.current_version                                                        AS version_app,
       ac.current_os                                                             AS version_os,
       ac.lifetested                                                             AS prueba_vida,
       pe.name                                                                   AS nombre_person,
       pe.last_name                                                              AS apellido_person,
       concat(pe.name, ' ', pe.last_name)                                        AS nombre_completo_person,
       pe.document_id                                                            AS documento_person,
       pe.birthdate                                                              AS fecha_nacimiento_person,
       pe.cuit                                                                   AS cuit_person,
       pe.is_exposed_person                                                      AS persona_expuesta,
       pe.address                                                                AS direccion_completa_person,
       CASE
           WHEN pe.address !~~ '%undefined%'::text AND "right"(pe.address, 6) ~~ '%,%'::text THEN "right"(pe.address, 4)
           ELSE '-'::text
           END                                                                   AS codpostal_real_person,
       COALESCE((((pe.location #>> '{}'::text[])::jsonb) -> 'location'::text) ->> 'lat'::text,
                ((pe.location #>> '{}'::text[])::jsonb) ->> 'lat'::text)         AS latitud_person,
       COALESCE((((pe.location #>> '{}'::text[])::jsonb) -> 'location'::text) ->> 'lng'::text,
                ((pe.location #>> '{}'::text[])::jsonb) ->> 'lng'::text)         AS longitud_person,
       ((pe.location #>> '{}'::text[])::jsonb) ->> 'postal_address'::text        AS codpostal_person,
       ((pe.location #>> '{}'::text[])::jsonb) ->> 'locality'::text              AS localidad_person,
       COALESCE(((pe.location #>> '{}'::text[])::jsonb) ->> 'department'::text,
                ((pe.location #>> '{}'::text[])::jsonb) ->> 'deparment'::text)   AS departamento_person,
       initcap(replace(replace(replace(((pe.location #>> '{}'::text[])::jsonb) ->> 'province'::text, ' Province'::text,
                                       ''::text), 'Provincia de Buenos Aires'::text, 'Buenos Aires'::text),
                       ' Department'::text, ''::text))                           AS provincia_person,
       co.name                                                                   AS "nombre_compañía",
       co.business_name                                                          AS "nombre_dueño",
       co.document                                                               AS "documento_compañía",
       bs.name                                                                   AS business_sector,
       co.address                                                                AS direccion_completa_company,
       COALESCE((((co.location #>> '{}'::text[])::jsonb) -> 'location'::text) ->> 'lat'::text,
                ((co.location #>> '{}'::text[])::jsonb) ->> 'lat'::text)         AS latitud_company,
       COALESCE((((co.location #>> '{}'::text[])::jsonb) -> 'location'::text) ->> 'lng'::text,
                ((co.location #>> '{}'::text[])::jsonb) ->> 'lng'::text)         AS longitud_company,
       concat(((co.location #>> '{}'::text[])::jsonb) ->> 'street'::text, ' ',
              ((co.location #>> '{}'::text[])::jsonb) ->> 'street_number'::text) AS direccion_company,
       ((co.location #>> '{}'::text[])::jsonb) ->> 'postal_address'::text        AS codpostal_company,
       ((co.location #>> '{}'::text[])::jsonb) ->> 'locality'::text              AS localidad_company,
       COALESCE(((co.location #>> '{}'::text[])::jsonb) ->> 'department'::text,
                ((co.location #>> '{}'::text[])::jsonb) ->> 'deparment'::text)   AS departamento_company,
       initcap(replace(replace(replace(((co.location #>> '{}'::text[])::jsonb) ->> 'province'::text, ' Province'::text,
                                       ''::text), 'Provincia de Buenos Aires'::text, 'Buenos Aires'::text),
                       ' Department'::text, ''::text))                           AS provincia_company
FROM app.account ac
         LEFT JOIN app.company co ON ac.account_id = co.account_id
         LEFT JOIN app.person pe ON ac.account_id = pe.account_id
         LEFT JOIN app.business_sector bs ON co.business_sector_id = bs.business_sector_id;

alter table bi."vw_full_account_OLD"
    owner to bi;

--grant select on bi."vw_full_account_OLD" to qs;

create view bi."vw_full_company_DELETE"
            (account_id, company_id, external_ref, nombre_compania, nombre_dueño, cuit_compania, email_compania,
             telefono_compania, tipo_persona, direccion_compania, codigo_postal, provincia, tipo_gross_income,
             tipo_negocio, status_compania, condicion_iva, condicion_ganancias, condicion_especial, activo_loyalty)
as
SELECT c.account_id,
       c.company_id,
       a.account_hash   AS external_ref,
       c.name           AS nombre_compania,
       c.business_name  AS "nombre_dueño",
       c.document       AS cuit_compania,
       a.email          AS email_compania,
       a.phone          AS telefono_compania,
       CASE
           WHEN "left"(c.document, 2) = '20'::text OR "left"(c.document, 2) = '23'::text OR
                "left"(c.document, 2) = '24'::text OR "left"(c.document, 2) = '27'::text THEN 'Persona física'::text
           WHEN "left"(c.document, 2) = '30'::text OR "left"(c.document, 2) = '33'::text OR "left"(c.document, 2) = '34'::text
               THEN 'Persona jurídica'::text
           ELSE NULL::text
           END          AS tipo_persona,
       c.address        AS direccion_compania,
       c.postal_address AS codigo_postal,
       p.name           AS provincia,
       g.name           AS tipo_gross_income,
       b.name           AS tipo_negocio,
       cs.name          AS status_compania,
       iv.name          AS condicion_iva,
       ec.name          AS condicion_ganancias,
       sc.name          AS condicion_especial,
       c.loyalty_active AS activo_loyalty
FROM app.company c
         LEFT JOIN app.account a ON c.account_id = a.account_id
         LEFT JOIN app.gross_income_type g ON c.gross_income_type_id = g.gross_income_type_id
         LEFT JOIN app.business_sector b ON c.business_sector_id = b.business_sector_id
         LEFT JOIN app.company_status cs ON c.company_status_id = cs.company_status_id
         LEFT JOIN app.iva_condition iv ON c.iva_condition_id = iv.iva_condition_id
         LEFT JOIN app.earning_condition ec ON c.earning_condition_id = ec.earning_condition_id
         LEFT JOIN app.special_condition sc ON c.special_condition_id = sc.special_condition_id
         LEFT JOIN app.province p ON c.province_id = p.province_id;

alter table bi."vw_full_company_DELETE"
    owner to bi;

--grant select on bi."vw_full_company_DELETE" to qs;

create view bi.vw_full_person
            (person_id, account_id, external_ref, nombre_persona, apellido_persona, nombre_completo_persona,
             cuit_persona, dni_persona, email_persona, telefono_persona, tipo_persona, direccion_persona, provincia,
             persona_expuesta, fecha_nacimiento_persona)
as
SELECT p.person_id,
       a.account_id,
       a.account_hash                   AS external_ref,
       p.name                           AS nombre_persona,
       p.last_name                      AS apellido_persona,
       concat(p.name, ' ', p.last_name) AS nombre_completo_persona,
       p.cuit                           AS cuit_persona,
       p.document_id                    AS dni_persona,
       a.email                          AS email_persona,
       a.phone                          AS telefono_persona,
       CASE
           WHEN "left"(p.cuit, 2) = '20'::text OR "left"(p.cuit, 2) = '23'::text OR "left"(p.cuit, 2) = '24'::text OR
                "left"(p.cuit, 2) = '27'::text THEN 'Persona física'::text
           WHEN "left"(p.cuit, 2) = '30'::text OR "left"(p.cuit, 2) = '33'::text OR "left"(p.cuit, 2) = '34'::text
               THEN 'Persona jurídica'::text
           ELSE NULL::text
           END                          AS tipo_persona,
       p.address                        AS direccion_persona,
       p.province                       AS provincia,
       p.is_exposed_person              AS persona_expuesta,
       p.birthdate                      AS fecha_nacimiento_persona
FROM app.person p
         LEFT JOIN app.account a ON p.account_id = a.account_id;

alter table bi.vw_full_person
    owner to bi;

--grant select on bi.vw_full_person to qs;

create view bi.vw_loyalty_full
            (dia_creación, fecha_creación, id_cliente, telefono_cliente, usuario_tap, id_local, account_id_local,
             nombre_local, cant_sellos_cliente_local, cant_rewards, cant_visitas)
as
WITH aux AS (
    SELECT er1.id,
           er1.reward_id,
           er1.client_id,
           er1.created_at,
           er1.updated_at,
           r1.id,
           r1.business_id,
           r1.name,
           r1.points,
           r1.created_at,
           r1.updated_at,
           r1.image,
           r1.description,
           r1.prize,
           er1.id AS id_posta
    FROM loyalty.exchanged_rewards er1
             LEFT JOIN loyalty.rewards r1 ON er1.reward_id = r1.id
)
SELECT to_char(p1.created_at, 'YYYY-MM-DD'::text) AS "dia_creación",
       p1.created_at                              AS "fecha_creación",
       p1.client_id                               AS id_cliente,
       CASE
           WHEN "left"(c1.phone_number::text, 3) = '549'::text THEN c1.phone_number::text
           ELSE concat('549', c1.phone_number)
           END                                    AS telefono_cliente,
       COALESCE(c1.has_tap, false)                AS usuario_tap,
       p1.business_id                             AS id_local,
       b1.external_business_id                    AS account_id_local,
       b1.name                                    AS nombre_local,
       p1.total_amount                            AS cant_sellos_cliente_local,
       count(DISTINCT aux.id_posta)               AS cant_rewards,
       count(DISTINCT v1.id)                      AS cant_visitas
FROM loyalty.points p1
         LEFT JOIN loyalty.clients c1 ON p1.client_id = c1.id
         LEFT JOIN loyalty.businesses b1 ON p1.business_id = b1.id
         LEFT JOIN loyalty.visits v1 ON concat(v1.client_id, v1.business_id) = concat(p1.client_id, p1.business_id)
         LEFT JOIN aux aux(id, reward_id, client_id, created_at, updated_at, id_1, business_id, name, points,
                           created_at_1, updated_at_1, image, description, prize, id_posta)
                   ON concat(aux.client_id, aux.business_id) = concat(p1.client_id, p1.business_id)
GROUP BY (to_char(p1.created_at, 'YYYY-MM-DD'::text)), p1.created_at, p1.client_id,
         (
             CASE
                 WHEN "left"(c1.phone_number::text, 3) = '549'::text THEN c1.phone_number::text
                 ELSE concat('549', c1.phone_number)
                 END), (COALESCE(c1.has_tap, false)), p1.business_id, b1.name, b1.external_business_id, p1.total_amount;

alter table bi.vw_loyalty_full
    owner to bi;

--grant select on bi.vw_loyalty_full to qs;

create view bi.vw_prepaid_card
            (card_id, account_id, card_created_at, card_type, card_status_id, card_number, reference, transaction_id,
             external_ref_pt, status, type, commerce_name, reason_id_cancel, created_at, external_ref_origin,
             provider_account_id)
as
SELECT c.id            AS card_id,
       c.account_id,
       c.created_at    AS card_created_at,
       c.card_type,
       c.card_status_id,
       c.number        AS card_number,
       c.reference,
       pt.id           AS transaction_id,
       pt.external_ref AS external_ref_pt,
       pt.status,
       pt.type,
       pt.commerce_name,
       pt.reason_id    AS reason_id_cancel,
       pt.created_at,
       o.external_ref  AS external_ref_origin,
       a.provider_account_id
FROM prepaid.card c
         LEFT JOIN prepaid.transaction pt ON c.id = pt.card_id
         LEFT JOIN prepaid.account a ON c.account_id = a.id
         LEFT JOIN prepaid.origin o ON a.origin_id = o.id;

alter table bi.vw_prepaid_card
    owner to bi;

--grant select on bi.vw_prepaid_card to qs;

create view bi.vw_prepaid_errores
            (updated_at, tx, service, external_ref, error_message, metadata_message, operation_status_id, card_type) as
SELECT o.updated_at,
       o.tx,
       o.service,
       o.external_ref,
       o.api_response ->> 'message'::text                       AS error_message,
       (o.api_response -> 'metadata'::text) ->> 'message'::text AS metadata_message,
       o.operation_status_id,
       0                                                        AS card_type
FROM prepaid.operation o
WHERE (o.operation_status_id = ANY (ARRAY [1::bigint, 3::bigint]))
  AND (o.service = ANY (ARRAY ['create-account-card'::text, 'external-authorization-request'::text]));

alter table bi.vw_prepaid_errores
    owner to bi;

--grant select on bi.vw_prepaid_errores to qs;

create view bi.vw_prepaid_sow (name, external_ref, mes_año, pago_servicios, pago_recargas, uso_prepaid) as
SELECT o1.name,
       o1.external_ref,
       to_char(m1.created_at, 'yyyy-mm'::text) AS "mes_año",
       CASE
           WHEN count(DISTINCT
                      CASE
                          WHEN t1.transaction_type_id = ANY (ARRAY [1, 22, 32, 33, 34]) THEN t1.transaction_id
                          ELSE NULL::text
                          END) > 0 THEN 1
           ELSE 0
           END                                 AS pago_servicios,
       CASE
           WHEN count(DISTINCT
                      CASE
                          WHEN t1.transaction_type_id = ANY (ARRAY [20, 21, 29]) THEN t1.transaction_id
                          ELSE NULL::text
                          END) > 0 THEN 1
           ELSE 0
           END                                 AS pago_recargas,
       CASE
           WHEN count(DISTINCT
                      CASE
                          WHEN t1.transaction_type_id = 30 THEN t1.transaction_id
                          ELSE NULL::text
                          END) > 0 THEN 1
           ELSE 0
           END                                 AS uso_prepaid
FROM ledger.movement m1
         LEFT JOIN ledger.transaction t1 ON m1.id = t1.movement_id
         LEFT JOIN ledger.origin o1 ON t1.origin_id = o1.origin_id
GROUP BY (to_char(m1.created_at, 'yyyy-mm'::text)), o1.external_ref, o1.name;

alter table bi.vw_prepaid_sow
    owner to bi;

--grant select on bi.vw_prepaid_sow to qs;

create view bi.vw_promotion_completa
            (created_at, tx, amount, origin_id, codigo, transaction, subtransaction, amount_percentage, max_amount,
             max_usage, fecha_creacion, fecha_activacion, fecha_finalizacion, active)
as
SELECT t.created_at,
       t.tx,
       t.amount,
       t.origin_id,
       t.codigo,
       t.transaction,
       t.subtransaction,
       t.amount_percentage,
       t.max_amount,
       t.max_usage,
       t.fecha_creacion,
       t.fecha_activacion,
       t.fecha_finalizacion,
       t.active
FROM (SELECT m1.created_at,
             m1.tx,
             m1.amount,
             m1.origin_id::integer                                                 AS origin_id,
             o1.message ->> 'code'::text                                           AS codigo,
             ((o1.message -> 'subTrxs'::text) -> 0) ->> 'transaction_id'::text     AS transaction,
             ((o1.message -> 'subTrxs'::text) -> 0) ->> 'sub_transaction_id'::text AS subtransaction,
             concat(p1.amount::text, '%')                                          AS amount_percentage,
             p1.max_amount,
             p1.max_usage,
             p1.created_at                                                         AS fecha_creacion,
             p1.activated_at                                                       AS fecha_activacion,
             p1.finished_at                                                        AS fecha_finalizacion,
             p1.active
      FROM promotion.operation o1
               LEFT JOIN promotion.movement m1 ON o1.tx = m1.tx
               LEFT JOIN promotion.promotion p1 ON (o1.message ->> 'code'::text) = p1.code) t;

alter table bi.vw_promotion_completa
    owner to bi;

--grant select on bi.vw_promotion_completa to qs;

create view bi.vw_promotion_complete
            (codigo, id_promo, amount_real, max_amount, amount_percentage, max_usage, fecha_creacion, fecha_activacion,
             fecha_finalizacion, activo, is_used, transaction, subtransaction, external_ref)
as
SELECT t.codigo,
       t.id_promo,
       t.amount_real,
       t.max_amount,
       t.amount_percentage,
       t.max_usage,
       t.fecha_creacion,
       t.fecha_activacion,
       t.fecha_finalizacion,
       t.activo,
       t.is_used,
       t.transaction,
       t.subtransaction,
       t.external_ref
FROM (SELECT p1.code                                                                               AS codigo,
             p1.id_promo,
             COALESCE(((o1.message -> 'source'::text) ->> 'amount'::text)::numeric, NULL::numeric) AS amount_real,
             p1.max_amount,
             concat(p1.amount::text, '%')                                                          AS amount_percentage,
             p1.max_usage,
             p1.created_at                                                                         AS fecha_creacion,
             p1.activated_at                                                                       AS fecha_activacion,
             p1.finished_at::date                                                                  AS fecha_finalizacion,
             p1.active                                                                             AS activo,
             CASE
                 WHEN COALESCE(((o1.message -> 'source'::text) ->> 'amount'::text)::numeric, NULL::numeric) > 0::numeric
                     THEN true
                 ELSE COALESCE((o1.message ->> 'is_used'::text)::boolean, false)
                 END                                                                               AS is_used,
             ((o1.message -> 'subTrxs'::text) -> 0) ->> 'transaction_id'::text                     AS transaction,
             ((o1.message -> 'subTrxs'::text) -> 0) ->> 'sub_transaction_id'::text                 AS subtransaction,
             ((((o1.message -> 'subTrxs'::text) -> 0) -> 'movement'::text) -> 'inserted'::text) ->>
             'external_ref'::text                                                                  AS external_ref
      FROM promotion.operation o1
               FULL JOIN promotion.promotion p1 ON (o1.message ->> 'code'::text) = p1.code) t
WHERE t.codigo IS NOT NULL;

alter table bi.vw_promotion_complete
    owner to bi;

--grant select on bi.vw_promotion_complete to qs;

create view bi.vw_ranking_usuarios
            (origin_name, extref_origin, origin_document, origin_type_ok, balance, cant_external_transfer,
             monto_external_transfer, cant_pago_servicio, monto_pago_servicio, cant_recarga_servicio,
             monto_recarga_servicio, cant_cashin_credito, monto_cashin_credito, cant_cashin_debito, monto_cashin_debito,
             cant_transfer_cbu, monto_transfer_cbu, cant_recarga_mide, monto_recarga_mide, cant_t2t_emisor,
             monto_t2t_emisor, cant_pago_qr, monto_pago_qr, cant_bonif_reintegros, monto_bonif_reintegros,
             cant_promocode, monto_promocode, cant_tx_totales, monto_tx_totales)
as
SELECT vw_transaction.origin_name,
       vw_transaction.extref_origin,
       vw_transaction.origin_document,
       vw_transaction.origin_type_ok,
       round(avg(vw_transaction.origin_balance::numeric), 2) AS balance,
       count(DISTINCT
             CASE
                 WHEN vw_transaction.description_ok = 'EXTERNAL TRANSFER'::text THEN vw_transaction.transaction
                 ELSE ''::text
                 END) - 1                                    AS cant_external_transfer,
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok = 'EXTERNAL TRANSFER'::text THEN vw_transaction.amount_abs
                         ELSE 0::numeric
                         END), 2)                            AS monto_external_transfer,
       count(DISTINCT
             CASE
                 WHEN vw_transaction.description_ok = 'SERVICE PAYMENT'::text OR
                      vw_transaction.description_ok = 'CARD_SERVICE_PAYMENT'::text THEN vw_transaction.transaction
                 ELSE ''::text
                 END) - 1                                    AS cant_pago_servicio,
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok = 'SERVICE PAYMENT'::text OR
                              vw_transaction.description_ok = 'CARD_SERVICE_PAYMENT'::text
                             THEN vw_transaction.amount_abs
                         ELSE 0::numeric
                         END), 2)                            AS monto_pago_servicio,
       count(DISTINCT
             CASE
                 WHEN vw_transaction.description_ok = 'RECHARGE_PAYMENT'::text OR
                      vw_transaction.description_ok = 'RECHARGE_CARD_PAYMENT'::text THEN vw_transaction.transaction
                 ELSE ''::text
                 END) - 1                                    AS cant_recarga_servicio,
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok = 'RECHARGE_PAYMENT'::text OR
                              vw_transaction.description_ok = 'RECHARGE_CARD_PAYMENT'::text
                             THEN vw_transaction.amount_abs
                         ELSE 0::numeric
                         END), 2)                            AS monto_recarga_servicio,
       count(DISTINCT
             CASE
                 WHEN vw_transaction.description_ok = 'CASH IN CREDIT CARD'::text THEN vw_transaction.transaction
                 ELSE ''::text
                 END) - 1                                    AS cant_cashin_credito,
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok = 'CASH IN CREDIT CARD'::text THEN vw_transaction.amount_abs
                         ELSE 0::numeric
                         END), 2)                            AS monto_cashin_credito,
       count(DISTINCT
             CASE
                 WHEN vw_transaction.description_ok = 'CASH IN DEBIT CARD'::text THEN vw_transaction.transaction
                 ELSE ''::text
                 END) - 1                                    AS cant_cashin_debito,
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok = 'CASH IN DEBIT CARD'::text THEN vw_transaction.amount_abs
                         ELSE 0::numeric
                         END), 2)                            AS monto_cashin_debito,
       count(DISTINCT
             CASE
                 WHEN vw_transaction.description_ok = 'TRANSFERS TO CBU'::text OR
                      vw_transaction.description_ok = 'TRANSFER TO CVU'::text OR
                      vw_transaction.description_ok = 'CARD_TRANSFER_TO_CBU'::text THEN vw_transaction.transaction
                 ELSE ''::text
                 END) - 1                                    AS cant_transfer_cbu,
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok = 'TRANSFERS TO CBU'::text OR
                              vw_transaction.description_ok = 'TRANSFER TO CVU'::text OR
                              vw_transaction.description_ok = 'CARD_TRANSFER_TO_CBU'::text
                             THEN vw_transaction.amount_abs
                         ELSE 0::numeric
                         END), 2)                            AS monto_transfer_cbu,
       count(DISTINCT
             CASE
                 WHEN vw_transaction.description_ok ~~ '%LIGHTNING_PAYMENT%'::text THEN vw_transaction.transaction
                 ELSE ''::text
                 END) - 1                                    AS cant_recarga_mide,
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok ~~ '%LIGHTNING_PAYMENT%'::text
                             THEN vw_transaction.amount_abs
                         ELSE 0::numeric
                         END), 2)                            AS monto_recarga_mide,
       count(DISTINCT
             CASE
                 WHEN vw_transaction.description_ok = 'TRANSFER TAP TO TAP'::text AND
                      vw_transaction.amount < 0::numeric AND vw_transaction.target_type_ok <> 'company'::text
                     THEN vw_transaction.transaction
                 ELSE ''::text
                 END) - 1                                    AS cant_t2t_emisor,
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok = 'TRANSFER TAP TO TAP'::text AND
                              vw_transaction.amount < 0::numeric AND vw_transaction.target_type_ok <> 'company'::text
                             THEN vw_transaction.amount_abs
                         ELSE 0::numeric
                         END), 2)                            AS monto_t2t_emisor,
       count(DISTINCT
             CASE
                 WHEN vw_transaction.description_ok = 'TRANSFER TAP TO TAP'::text AND
                      vw_transaction.is_qr = 'true'::text AND vw_transaction.target_type_ok = 'company'::text AND
                      vw_transaction.amount_abs >= 50::numeric THEN vw_transaction.transaction
                 ELSE ''::text
                 END) - 1                                    AS cant_pago_qr,
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok = 'TRANSFER TAP TO TAP'::text AND
                              vw_transaction.is_qr = 'true'::text AND
                              vw_transaction.target_type_ok = 'company'::text AND vw_transaction.amount_abs >= 50::numeric
                             THEN vw_transaction.amount_abs
                         ELSE 0::numeric
                         END), 2)                            AS monto_pago_qr,
       count(DISTINCT
             CASE
                 WHEN vw_transaction.description_ok = 'BONIF/REINTEGROS'::text THEN vw_transaction.transaction
                 ELSE ''::text
                 END) - 1                                    AS cant_bonif_reintegros,
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok = 'BONIF/REINTEGROS'::text THEN vw_transaction.amount_abs
                         ELSE 0::numeric
                         END), 2)                            AS monto_bonif_reintegros,
       count(DISTINCT
             CASE
                 WHEN vw_transaction.description_ok = 'PROMOTION CODE'::text THEN vw_transaction.transaction
                 ELSE ''::text
                 END) - 1                                    AS cant_promocode,
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok = 'PROMOTION CODE'::text THEN vw_transaction.amount_abs
                         ELSE 0::numeric
                         END), 2)                            AS monto_promocode,
       count(DISTINCT vw_transaction.transaction)            AS cant_tx_totales,
       round(sum(vw_transaction.amount_abs), 2)              AS monto_tx_totales
FROM bi.vw_transaction
WHERE vw_transaction.extref_origin <> 'tap-wc'::text
GROUP BY vw_transaction.origin_name, vw_transaction.extref_origin, vw_transaction.origin_document,
         vw_transaction.origin_type_ok;

alter table bi.vw_ranking_usuarios
    owner to bi;

--grant select on bi.vw_ranking_usuarios to qs;

create view bi.vw_recargas
            (created_at, transaction_hour, transaction_day, external_ref, target, medidor_mide, comprobante_mide,
             celular_recargado, name, email)
as
SELECT m.created_at,
       to_char(m.created_at - '00:00:00'::time without time zone::interval, 'HH24:MI:SS'::text)        AS transaction_hour,
       to_char(m.created_at - '00:00:00'::time without time zone::interval,
               'YYYY-MM-DD'::text)                                                                     AS transaction_day,
       o.external_ref,
       m.target,
       split_part(split_part((m.metadata -> 'ticket'::text) ->> 'Ticket'::text, 'Medidor Nro. '::text, 2), '"'::text,
                  1)                                                                                   AS medidor_mide,
       m.metadata ->> 'last_token'::text                                                               AS comprobante_mide,
       ((m.metadata -> 'operation'::text) -> 'operation_response'::text) ->>
       'company_client_id'::text                                                                       AS celular_recargado,
       o.name,
       ac.email
FROM ledger.transaction t
         LEFT JOIN ledger.origin o USING (origin_id)
         LEFT JOIN ledger.movement m ON m.id = t.movement_id
         LEFT JOIN app.account ac ON o.external_ref = ac.account_hash
WHERE (t.transaction_type_id = ANY (ARRAY [20, 21, 29]))
  AND (m.target = ANY (ARRAY ['PERSONAL'::text, 'CLARO'::text, 'MOVISTAR'::text, 'tuenti'::text, 'MIDE'::text]));

alter table bi.vw_recargas
    owner to bi;

--grant select on bi.vw_recargas to qs;

create view bi.vw_retencion_mide
            (external_ref, creation_date, primera_tx_mide, ext_ref_1mes, ext_ref_2mes, ext_ref_3mes, ext_ref_4mes,
             ext_ref_5mes, ext_ref_6mes)
as
SELECT t.external_ref,
       t.fecha_creacion_cuenta::date AS creation_date,
       t.primera_tx_mide::date       AS primera_tx_mide,
       CASE
           WHEN count(DISTINCT
                      CASE
                          WHEN t.description_ok = 'LIGHTNING_PAYMENT'::text AND
                               t.transaction_date >= (t.primera_tx_mide + '00:00:01'::interval second) AND
                               t.transaction_date <= (t.primera_tx_mide + '30 days'::interval day) THEN t.transaction
                          ELSE NULL::text
                          END) > 0 THEN t.external_ref
           ELSE NULL::text
           END                       AS ext_ref_1mes,
       CASE
           WHEN count(DISTINCT
                      CASE
                          WHEN t.description_ok = 'LIGHTNING_PAYMENT'::text AND
                               t.transaction_date >= (t.primera_tx_mide + '31 days'::interval day) AND
                               t.transaction_date <= (t.primera_tx_mide + '60 days'::interval day) THEN t.transaction
                          ELSE NULL::text
                          END) > 0 THEN t.external_ref
           ELSE NULL::text
           END                       AS ext_ref_2mes,
       CASE
           WHEN count(DISTINCT
                      CASE
                          WHEN t.description_ok = 'LIGHTNING_PAYMENT'::text AND
                               t.transaction_date >= (t.primera_tx_mide + '61 days'::interval day) AND
                               t.transaction_date <= (t.primera_tx_mide + '90 days'::interval day) THEN t.transaction
                          ELSE NULL::text
                          END) > 0 THEN t.external_ref
           ELSE NULL::text
           END                       AS ext_ref_3mes,
       CASE
           WHEN count(DISTINCT
                      CASE
                          WHEN t.description_ok = 'LIGHTNING_PAYMENT'::text AND
                               t.transaction_date >= (t.primera_tx_mide + '91 days'::interval day) AND
                               t.transaction_date <= (t.primera_tx_mide + '120 days'::interval day) THEN t.transaction
                          ELSE NULL::text
                          END) > 0 THEN t.external_ref
           ELSE NULL::text
           END                       AS ext_ref_4mes,
       CASE
           WHEN count(DISTINCT
                      CASE
                          WHEN t.description_ok = 'LIGHTNING_PAYMENT'::text AND
                               t.transaction_date >= (t.primera_tx_mide + '121 days'::interval day) AND
                               t.transaction_date <= (t.primera_tx_mide + '150 days'::interval day) THEN t.transaction
                          ELSE NULL::text
                          END) > 0 THEN t.external_ref
           ELSE NULL::text
           END                       AS ext_ref_5mes,
       CASE
           WHEN count(DISTINCT
                      CASE
                          WHEN t.description_ok = 'LIGHTNING_PAYMENT'::text AND
                               t.transaction_date >= (t.primera_tx_mide + '151 days'::interval day) AND
                               t.transaction_date <= (t.primera_tx_mide + '180 days'::interval day) THEN t.transaction
                          ELSE NULL::text
                          END) > 0 THEN t.external_ref
           ELSE NULL::text
           END                       AS ext_ref_6mes
FROM (WITH tabla AS (
    SELECT r.external_ref,
           min(r.transaction_date) AS primera_tx_mide
    FROM (SELECT vw_transaction.extref_origin AS external_ref,
                 vw_transaction.transaction,
                 vw_transaction.description_ok,
                 vw_transaction.transaction_date,
                 vw_transaction.tx_activa,
                 vw_transaction.fecha_tx_activa
          FROM bi.vw_transaction
          WHERE vw_transaction.description_ok = 'LIGHTNING_PAYMENT'::text) r
    GROUP BY r.external_ref
)
      SELECT tr.transaction,
             tr.transaction_id,
             tr.transaction_date,
             tr.transaction_dia,
             tr.transaction_hour,
             tr.movement_tx,
             tr.movement_id,
             tr.description_ok,
             tr.amount,
             tr.amount_abs,
             tr.is_qr,
             tr.details,
             tr.origin_id,
             tr.extref_origin,
             tr.origin_name,
             tr.origin_document,
             tr.origin_type_ok,
             tr.tx_origin_id,
             tr.tx_extref_origin,
             tr.tx_origin_name,
             tr.tx_origin_document,
             tr.tx_origin_type_ok,
             tr.origin_balance,
             tr.origin_tipo_persona,
             tr.tipo_tx,
             tr.fecha_creacion_cuenta,
             tr.fecha_eliminacion_cuenta,
             tr.target,
             tr.target_name,
             tr.target_document,
             tr.target_type_ok,
             tr.private,
             tr.t2t_emisor_extref,
             tr.t2t_emisor_nombre,
             tr.t2t_emisor_document,
             tr."mes_año",
             tr.tx_activa,
             tr.fecha_tx_activa,
             tabla.external_ref,
             tabla.primera_tx_mide
      FROM bi.vw_transaction tr
               LEFT JOIN tabla ON tr.extref_origin = tabla.external_ref) t
WHERE t.description_ok = 'LIGHTNING_PAYMENT'::text
GROUP BY t.external_ref, (t.fecha_creacion_cuenta::date), (t.primera_tx_mide::date);

alter table bi.vw_retencion_mide
    owner to bi;

--grant select on bi.vw_retencion_mide to qs;

create view bi.vw_rewards
            (dia_reward, fecha_reward, id_reward, id_cliente, telefono_cliente, usuario_tap, id_local, account_id_local,
             nombre_local, reward_id, description_reward)
as
WITH aux AS (
    SELECT er1.id         AS id_posta,
           er1.client_id  AS id_cliente,
           er1.reward_id,
           er1.created_at AS fecha_reward,
           r1.business_id AS id_local,
           r1.description AS description_reward
    FROM loyalty.exchanged_rewards er1
             LEFT JOIN loyalty.rewards r1 ON er1.reward_id = r1.id
)
SELECT to_char(aux.fecha_reward, 'YYYY-MM-DD'::text) AS dia_reward,
       aux.fecha_reward,
       aux.id_posta                                  AS id_reward,
       aux.id_cliente,
       CASE
           WHEN "left"(c1.phone_number::text, 3) = '549'::text THEN c1.phone_number::text
           ELSE concat('549', c1.phone_number)
           END                                       AS telefono_cliente,
       COALESCE(c1.has_tap, false)                   AS usuario_tap,
       aux.id_local,
       b1.external_business_id                       AS account_id_local,
       b1.name                                       AS nombre_local,
       aux.reward_id,
       aux.description_reward
FROM aux aux
         LEFT JOIN loyalty.clients c1 ON aux.id_cliente = c1.id
         LEFT JOIN loyalty.businesses b1 ON aux.id_local = b1.id;

alter table bi.vw_rewards
    owner to bi;

--grant select on bi.vw_rewards to qs;

create view bi.vw_sellos
            (dia_sello, fecha_sello, id_sello, id_cliente, telefono_cliente, usuario_tap, id_local, account_id_local,
             nombre_local, earned_points, email_local, telefono_local, direccion_local, cuit_local, client_hash)
as
SELECT to_char(v1.created_at, 'YYYY-MM-DD'::text) AS dia_sello,
       v1.created_at                              AS fecha_sello,
       v1.id                                      AS id_sello,
       v1.client_id                               AS id_cliente,
       CASE
           WHEN "left"(c1.phone_number::text, 3) = '549'::text THEN c1.phone_number::text
           ELSE concat('549', c1.phone_number)
           END                                    AS telefono_cliente,
       COALESCE(c1.has_tap, false)                AS usuario_tap,
       v1.business_id                             AS id_local,
       b1.external_business_id                    AS account_id_local,
       b1.name                                    AS nombre_local,
       v1.earned_points,
       acc.email                                  AS email_local,
       acc.phone                                  AS telefono_local,
       acc.address                                AS direccion_local,
       acc.document                               AS cuit_local,
       v1.client_hash
FROM loyalty.visits v1
         LEFT JOIN loyalty.clients c1 ON v1.client_id = c1.id
         LEFT JOIN loyalty.businesses b1 ON v1.business_id = b1.id
         LEFT JOIN (SELECT account.account_id,
                           account.username,
                           account.password,
                           account.email,
                           account.phone,
                           account.oauth_token,
                           account.qr_image,
                           account.device_id,
                           account.account_role_id,
                           account.account_hash,
                           account.current_version,
                           account.current_os,
                           account.lifetested,
                           account.account_type_id,
                           account.failed_login_attempts,
                           company.company_id,
                           company.account_id,
                           company.name,
                           company.business_name,
                           company.document,
                           company.address,
                           company.address_additional,
                           company.location,
                           company.business_sector_id,
                           company.gross_income_type_id,
                           company.gross_income_number,
                           company.company_status_id,
                           company.files_url,
                           company.profile_picture_url,
                           company.summary_name,
                           company.iva_condition_id,
                           company.earning_condition_id,
                           company.special_condition_id,
                           company.loyalty_integration_token,
                           company.loyalty_active,
                           company.province_id,
                           company.postal_address,
                           company.is_exposed_person
                    FROM app.account
                             LEFT JOIN app.company ON account.account_id = company.account_id) acc(account_id, username,
                                                                                                   password, email,
                                                                                                   phone, oauth_token,
                                                                                                   qr_image, device_id,
                                                                                                   account_role_id,
                                                                                                   account_hash,
                                                                                                   current_version,
                                                                                                   current_os,
                                                                                                   lifetested,
                                                                                                   account_type_id,
                                                                                                   failed_login_attempts,
                                                                                                   company_id,
                                                                                                   account_id_1, name,
                                                                                                   business_name,
                                                                                                   document, address,
                                                                                                   address_additional,
                                                                                                   location,
                                                                                                   business_sector_id,
                                                                                                   gross_income_type_id,
                                                                                                   gross_income_number,
                                                                                                   company_status_id,
                                                                                                   files_url,
                                                                                                   profile_picture_url,
                                                                                                   summary_name,
                                                                                                   iva_condition_id,
                                                                                                   earning_condition_id,
                                                                                                   special_condition_id,
                                                                                                   loyalty_integration_token,
                                                                                                   loyalty_active,
                                                                                                   province_id,
                                                                                                   postal_address,
                                                                                                   is_exposed_person)
                   ON b1.external_business_id = acc.company_id;

alter table bi.vw_sellos
    owner to bi;

--grant select on bi.vw_sellos to qs;

create view bi.vw_sellos_rewards
            (dia, id_sello, fecha_sello, cliente_sello, telef_cliente_sello, usuario_tap_sello, id_local_sello,
             nombre_local_sello, fecha_reward, id_reward, cliente_reward, telef_cliente_reward, id_local_reward,
             nombre_local_reward, reward_id, description_reward)
as
SELECT t.dia,
       t.id_sello,
       t.fecha_sello,
       t.cliente_sello,
       t.telef_cliente_sello,
       t.usuario_tap_sello,
       t.id_local_sello,
       t.nombre_local_sello,
       t.fecha_reward,
       t.id_reward,
       t.cliente_reward,
       t.telef_cliente_reward,
       t.id_local_reward,
       t.nombre_local_reward,
       t.reward_id,
       t.description_reward
FROM (WITH tabla_dias AS (
    SELECT dia.dia::date AS dia
    FROM generate_series('2020-10-01'::date::timestamp with time zone, now()::date::timestamp with time zone,
                         '1 day'::interval) dia(dia)
)
      SELECT td.dia,
             se.id_sello,
             se.fecha_sello,
             se.id_cliente                  AS cliente_sello,
             se.telefono_cliente            AS telef_cliente_sello,
             se.usuario_tap                 AS usuario_tap_sello,
             se.id_local                    AS id_local_sello,
             se.nombre_local                AS nombre_local_sello,
             NULL::timestamp with time zone AS fecha_reward,
             NULL::integer                  AS id_reward,
             NULL::integer                  AS cliente_reward,
             NULL::text                     AS telef_cliente_reward,
             NULL::integer                  AS id_local_reward,
             NULL::character varying        AS nombre_local_reward,
             NULL::integer                  AS reward_id,
             NULL::text                     AS description_reward
      FROM bi.vw_sellos se
               FULL JOIN tabla_dias td ON se.dia_sello::date = td.dia
      UNION
      SELECT td.dia,
             NULL::integer                  AS fecha_sello,
             NULL::timestamp with time zone AS id_sello,
             NULL::integer                  AS cliente_sello,
             NULL::text                     AS telef_cliente_sello,
             NULL::boolean                  AS usuario_tap_sello,
             NULL::integer                  AS id_local_sello,
             NULL::character varying        AS nombre_local_sello,
             rw.fecha_reward,
             rw.id_reward,
             rw.id_cliente                  AS cliente_reward,
             rw.telefono_cliente            AS telef_cliente_reward,
             rw.id_local                    AS id_local_reward,
             rw.nombre_local                AS nombre_local_reward,
             rw.reward_id,
             rw.description_reward
      FROM bi.vw_rewards rw
               FULL JOIN tabla_dias td ON rw.dia_reward::date = td.dia
      ORDER BY 1) t;

alter table bi.vw_sellos_rewards
    owner to bi;

--grant select on bi.vw_sellos_rewards to qs;

create view bi.vw_transaction_beyond
            (transaction_id, movement_id, description, transaction_hour, transaction_day, transaction_date, amount,
             origin_id, target, transaction_type_id, payment_key, pay_instrument, external_ref, name, email, phone,
             account_creation_date, first_transaction, private, source, id_billetera, qr_id_trx, cuit_comprador)
as
WITH first AS (
    SELECT t.origin_id,
           min(t.created_at) AS first_transaction
    FROM ledger.transaction t
             LEFT JOIN ledger.movement m ON t.movement_id = m.id
    WHERE (t.transaction_type_id = ANY (ARRAY [1, 20, 21, 22, 30, 31, 32, 33, 34]))
       OR t.transaction_type_id = 5 AND m.amount < 0::numeric
    GROUP BY t.origin_id
)
SELECT CASE
           WHEN m1.origin_type = 'MANUAL'::text THEN concat(m1.id, 'D-', m1.tx)
           ELSE t1.transaction_id
           END                                                                                   AS transaction_id,
       m1.id                                                                                     AS movement_id,
       CASE
           WHEN tt.description = 'WORKER ACCOUNT BALANCER'::text AND lower(m1.details) ~~ '%a tu cvu%'::text
               THEN 'EXTERNAL TRANSFER'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%a tu cvu%'::text
               THEN 'EXTERNAL TRANSFER'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%ajuste%'::text
               THEN 'WORKER ACCOUNT BALANCER'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%transferencia manual%'::text
               THEN 'TRANSFER TAP TO TAP'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%te transfi%'::text
               THEN 'TRANSFER TAP TO TAP'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%fue transfe%'::text
               THEN 'TRANSFER TAP TO TAP'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND
                lower(m1.details) ~~ '%acreditado desde la tarjeta%'::text AND m1.details ~~ '%bito%'::text
               THEN 'CASH IN DEBIT CARD'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND
                lower(m1.details) ~~ '%acreditado desde la tarjeta%'::text AND m1.details !~~ '%bito%'::text
               THEN 'CASH IN CREDIT CARD'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%transferiste a%'::text
               THEN 'TRANSFERS TO CBU'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%cash in a la wc%'::text
               THEN 'WC CASH IN'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%la cuenta wc%'::text AND
                lower(m1.details) ~~ '%fondeo%'::text THEN 'WC_FOUND'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%worker concilia%'::text
               THEN 'WORKER ACCOUNT BALANCER'::text
           WHEN lower(m1.details) ~~ '%bonifica%'::text OR lower(m1.details) ~~ '%reintegro%'::text OR
                lower(m1.details) ~~ '%devoluc%'::text OR lower(m1.details) ~~ '%reembols%'::text
               THEN 'BONIF/REINTEGROS'::text
           ELSE tt.description
           END                                                                                   AS description,
       to_char(m1.created_at - '00:00:00'::time without time zone::interval, 'HH24:MI:SS'::text) AS transaction_hour,
       to_char(m1.created_at - '00:00:00'::time without time zone::interval, 'YYYY-MM-DD'::text) AS transaction_day,
       m1.created_at - '00:00:00'::time without time zone::interval                              AS transaction_date,
       m1.amount,
       m1.origin_id,
       m1.target,
       t1.transaction_type_id,
       p1.payment_key,
       p1.pay_instrument,
       o1.external_ref,
       o1.name,
       a1.email,
       a1.phone,
       a1.created_at - '03:00:00'::time without time zone::interval                              AS account_creation_date,
       f1.first_transaction - '03:00:00'::time without time zone::interval                       AS first_transaction,
       m1.private,
       p1.source,
       ((qr.metadata -> 'operacion'::text) -> 'detalle'::text) ->> 'idBilletera'::text           AS id_billetera,
       qr.qr_id_trx,
       qr.cuit_comprador
FROM ledger.movement m1
         LEFT JOIN ledger.transaction t1 ON t1.movement_id = m1.id
         LEFT JOIN ledger.transaction_type tt ON t1.transaction_type_id = tt.transaction_type_id
         LEFT JOIN beyond.payments p1 ON (m1.metadata ->> 'idTransaccion'::text) = p1.transaction_id::text
         LEFT JOIN ledger.origin o1 ON m1.origin_id::integer = o1.origin_id
         LEFT JOIN app.account a1 ON o1.external_ref = a1.account_hash
         LEFT JOIN fiat.confirm_qr_debit qr
                   ON (((m1.metadata -> 'payload'::text) -> 'data'::text) ->> 'id'::text) = qr.id_debin::text
         LEFT JOIN first f1 ON m1.origin_id::integer = f1.origin_id
WHERE date(m1.created_at) > '2020-12-31'::date;

alter table bi.vw_transaction_beyond
    owner to bi;

--grant select on bi.vw_transaction_beyond to qs;

create view bi.vw_transaction_prepaid
            (card_id, account_id, card_created_at, card_type, card_status_id, card_number, transaction_id, description,
             transaction_type_id, transaction_hour, transaction_day, movement_id, transaction_date, amount, origin_id,
             target, private, isinternational, external_ref, name, email, phone, account_creation_date,
             first_transaction, external_ref_pt, status, type, commerce_name, reason_id_cancel, provider_account_id,
             first_transaction_hour, first_transaction_day, edad, has_virtual_prepaid_card)
as
WITH first AS (
    SELECT t.origin_id,
           min(m.created_at) AS first_transaction
    FROM ledger.transaction t
             LEFT JOIN ledger.movement m ON t.movement_id = m.id
    WHERE (t.transaction_type_id = ANY (ARRAY [1, 20, 21, 22, 30, 31, 32, 33, 34]))
       OR t.transaction_type_id = 5 AND m.amount < 0::numeric
    GROUP BY t.origin_id
)
SELECT c.id                                                                                                          AS card_id,
       c.account_id,
       c.created_at                                                                                                  AS card_created_at,
       c.card_type,
       c.card_status_id,
       c.number                                                                                                      AS card_number,
       CASE
           WHEN m1.origin_type = 'MANUAL'::text THEN concat(m1.id, 'D-', m1.tx)
           ELSE t1.transaction_id
           END                                                                                                       AS transaction_id,
       CASE
           WHEN tt.description = 'WORKER ACCOUNT BALANCER'::text AND lower(m1.details) ~~ '%a tu cvu%'::text
               THEN 'EXTERNAL TRANSFER'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%a tu cvu%'::text
               THEN 'EXTERNAL TRANSFER'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%ajuste%'::text
               THEN 'WORKER ACCOUNT BALANCER'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%transferencia manual%'::text
               THEN 'TRANSFER TAP TO TAP'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%te transfi%'::text
               THEN 'TRANSFER TAP TO TAP'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%fue transfe%'::text
               THEN 'TRANSFER TAP TO TAP'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND
                lower(m1.details) ~~ '%acreditado desde la tarjeta%'::text AND m1.details ~~ '%bito%'::text
               THEN 'CASH IN DEBIT CARD'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND
                lower(m1.details) ~~ '%acreditado desde la tarjeta%'::text AND m1.details !~~ '%bito%'::text
               THEN 'CASH IN CREDIT CARD'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%transferiste a%'::text
               THEN 'TRANSFERS TO CBU'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%cash in a la wc%'::text
               THEN 'WC CASH IN'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%la cuenta wc%'::text AND
                lower(m1.details) ~~ '%fondeo%'::text THEN 'WC_FOUND'::text
           WHEN m1.id IS NOT NULL AND t1.transaction_id IS NULL AND lower(m1.details) ~~ '%worker concilia%'::text
               THEN 'WORKER ACCOUNT BALANCER'::text
           WHEN lower(m1.details) ~~ '%bonifica%'::text OR lower(m1.details) ~~ '%reintegro%'::text OR
                lower(m1.details) ~~ '%devoluc%'::text OR lower(m1.details) ~~ '%reembols%'::text
               THEN 'BONIF/REINTEGROS'::text
           ELSE tt.description
           END                                                                                                       AS description,
       t1.transaction_type_id,
       to_char(m1.created_at - '00:00:00'::time without time zone::interval,
               'HH24:MI:SS'::text)                                                                                   AS transaction_hour,
       to_char(m1.created_at - '00:00:00'::time without time zone::interval,
               'YYYY-MM-DD'::text)                                                                                   AS transaction_day,
       m1.id                                                                                                         AS movement_id,
       m1.created_at - '00:00:00'::time without time zone::interval                                                  AS transaction_date,
       m1.amount,
       m1.origin_id,
       m1.target,
       m1.private,
       m1.metadata ->> 'isInternational'::text                                                                       AS isinternational,
       o1.external_ref,
       o1.name,
       a1.email,
       a1.phone,
       a1.created_at - '00:00:00'::time without time zone::interval                                                  AS account_creation_date,
       f1.first_transaction - '00:00:00'::time without time zone::interval                                           AS first_transaction,
       pt.external_ref                                                                                               AS external_ref_pt,
       pt.status,
       pt.type,
       pt.commerce_name,
       pt.reason_id                                                                                                  AS reason_id_cancel,
       pa.provider_account_id,
       to_char(f1.first_transaction - '00:00:00'::time without time zone::interval,
               'HH24:MI:SS'::text)                                                                                   AS first_transaction_hour,
       to_char(f1.first_transaction - '00:00:00'::time without time zone::interval,
               'YYYY-MM-DD'::text)                                                                                   AS first_transaction_day,
       date_part('year'::text, age(CURRENT_DATE::timestamp with time zone,
                                   ap1.birthdate::timestamp with time zone))                                         AS edad,
       a1.has_virtual_prepaid_card
FROM ledger.movement m1
         LEFT JOIN ledger.transaction t1 ON t1.movement_id = m1.id
         LEFT JOIN ledger.transaction_type tt ON t1.transaction_type_id = tt.transaction_type_id
         LEFT JOIN prepaid.transaction pt ON pt.external_request_id = (m1.metadata ->> 'externalRequestId'::text)
         LEFT JOIN ledger.origin o1 ON m1.origin_id::integer = o1.origin_id
         LEFT JOIN prepaid.card c ON c.id = pt.card_id
         LEFT JOIN prepaid.account pa ON c.account_id = pa.id
         LEFT JOIN app.account a1 ON o1.external_ref = a1.account_hash
         LEFT JOIN app.person ap1 ON a1.account_id = ap1.account_id
         LEFT JOIN first f1 ON m1.origin_id::integer = f1.origin_id
WHERE date(m1.created_at) > '2020-12-31'::date;

alter table bi.vw_transaction_prepaid
    owner to bi;

--grant select on bi.vw_transaction_prepaid to qs;

create view bi.vw_users
            (external_ref, nombre, apellido, cuit, edad, sexo, ubicacion, saldo_en_cuenta, recarga, servicios, mide,
             localidad, province)
as
SELECT ac.account_hash                                                                                              AS external_ref,
       ap.name                                                                                                      AS nombre,
       ap.last_name                                                                                                 AS apellido,
       ap.cuit,
       date_part('year'::text, age(CURRENT_DATE::timestamp with time zone,
                                   ap.birthdate::timestamp with time zone))                                         AS edad,
       CASE
           WHEN "left"(ap.cuit, 2) = '20'::text THEN 'Hombre'::text
           WHEN "left"(ap.cuit, 2) = '27'::text THEN 'Mujer'::text
           ELSE 'N/A'::text
           END                                                                                                      AS sexo,
       ap.province                                                                                                  AS ubicacion,
       lo.balance                                                                                                   AS saldo_en_cuenta,
       CASE
           WHEN (lo.origin_id IN (SELECT DISTINCT lt.origin_id
                                  FROM ledger.transaction lt
                                  WHERE lt.transaction_type_id = ANY (ARRAY [20, 21]))) THEN '1'::text
           ELSE '0'::text
           END                                                                                                      AS recarga,
       CASE
           WHEN (lo.origin_id IN (SELECT DISTINCT lt.origin_id
                                  FROM ledger.transaction lt
                                  WHERE lt.transaction_type_id = ANY (ARRAY [1, 22]))) THEN '1'::text
           ELSE '0'::text
           END                                                                                                      AS servicios,
       CASE
           WHEN (lo.origin_id IN (SELECT DISTINCT lt.origin_id
                                  FROM ledger.transaction lt
                                  WHERE lt.transaction_type_id = 29)) THEN '1'::text
           ELSE '0'::text
           END                                                                                                      AS mide,
       al.locality                                                                                                  AS localidad,
       pr.name                                                                                                      AS province
FROM app.person ap
         LEFT JOIN app.account ac ON ap.account_id = ac.account_id
         LEFT JOIN ledger.origin lo ON ac.account_hash = lo.external_ref
         LEFT JOIN app.account_location al ON ac.account_id = al.account_id
         LEFT JOIN app.province pr ON al.province_id = pr.province_id;

alter table bi.vw_users
    owner to bi;

--grant select on bi.vw_users to qs;

create view bi."vw_wc_movements_details_moveid_DELETE"
            (dia_hora, movement_id, details, wc_cashin, wc_fund, wc_total, wc_acumulado) as
WITH cte AS (
    SELECT '00 - Valor inicial'::text                     AS dia_hora,
           0                                              AS movement_id,
           '-'::text                                      AS details,
           0                                              AS wc_cashin,
           0                                              AS wc_fund,
           '-1370737.43'::numeric + 5599.288 + 1642507.41 AS wc_total
    FROM ledger.transaction
    UNION
    SELECT to_char(vw_transaction.transaction_date, 'YYYY-MM-DD HH24:MI'::text) AS dia_hora,
           vw_transaction.movement_id,
           vw_transaction.details,
           sum(
                   CASE
                       WHEN vw_transaction.origin_name = 'TAP BILLETERA SA'::text AND vw_transaction.amount > 0::numeric
                           THEN vw_transaction.amount
                       ELSE 0::numeric
                       END)                                                     AS wc_cashin,
           sum(
                   CASE
                       WHEN vw_transaction.origin_name = 'TAP BILLETERA SA'::text AND vw_transaction.amount < 0::numeric
                           THEN vw_transaction.amount
                       ELSE 0::numeric
                       END)                                                     AS wc_fund,
           sum(
                   CASE
                       WHEN vw_transaction.origin_name = 'TAP BILLETERA SA'::text THEN vw_transaction.amount
                       ELSE 0::numeric
                       END)                                                     AS wc_total
    FROM bi.vw_transaction
    WHERE vw_transaction.origin_name = 'TAP BILLETERA SA'::text
    GROUP BY vw_transaction.transaction_date, vw_transaction.movement_id, vw_transaction.details
),
     cteranked AS (
         SELECT cte.dia_hora,
                cte.movement_id,
                cte.details,
                sum(cte.wc_cashin)                           AS wc_cashin,
                sum(cte.wc_fund)                             AS wc_fund,
                sum(cte.wc_total)                            AS wc_total,
                row_number() OVER (ORDER BY cte.movement_id) AS rownum
         FROM cte
         GROUP BY cte.dia_hora, cte.movement_id, cte.details
     )
SELECT c1.dia_hora,
       c1.movement_id,
       c1.details,
       c1.wc_cashin,
       c1.wc_fund,
       c1.wc_total,
       (SELECT round(sum(c2.wc_total), 2) AS round
        FROM cteranked c2
        WHERE c2.rownum <= c1.rownum) AS wc_acumulado
FROM cteranked c1;

alter table bi."vw_wc_movements_details_moveid_DELETE"
    owner to bi;

--grant select on bi."vw_wc_movements_details_moveid_DELETE" to qs;

create view bi.vw_wc_mov_details_resumen_id
            (dia_hora, dia, movement_id, details, resumen_mov, wc_cashin, wc_fund, wc_total, wc_acumulado) as
SELECT t.dia_hora,
       t.dia_hora::date AS dia,
       t.movement_id,
       t.details,
       CASE
           WHEN lower(t.details) ~~ '%fondeo%'::text THEN 'WC: FONDEO'::text
           WHEN lower(t.details) ~~ '%cash in a%'::text THEN 'WC: CASH IN'::text
           WHEN lower(t.details) ~~ '%enviaste a%'::text THEN 'WC: ENVÍA DINERO'::text
           WHEN lower(t.details) ~~ '%iva%'::text OR lower(t.details) ~~ '%ganancias%'::text OR
                lower(t.details) ~~ '%iibb%'::text THEN 'IMPUESTOS'::text
           WHEN lower(t.details) ~~ '%comisi%'::text THEN 'COMISIONES'::text
           WHEN lower(t.details) ~~ '%bonif%'::text OR lower(t.details) ~~ '%reinteg%'::text OR
                lower(t.details) ~~ '%devoluc%'::text THEN 'WC: PAGO BONIF/REINTEGROS/DEVOLUC'::text
           WHEN lower(t.details) ~~ '%worker%'::text THEN 'WC: CONCILIACIÓN'::text
           WHEN lower(t.details) ~~ '%ajuste%'::text THEN 'WC: AJUSTES'::text
           ELSE 'OTROS'::text
           END          AS resumen_mov,
       t.wc_cashin,
       t.wc_fund,
       t.wc_total,
       t.wc_acumulado
FROM bi."vw_wc_movements_details_moveid_DELETE" t
WHERE t.dia_hora <> '00 - Valor inicial'::text;

alter table bi.vw_wc_mov_details_resumen_id
    owner to bi;

--grant select on bi.vw_wc_mov_details_resumen_id to qs;

create view bi.vw_wc_movements (dia, wc_extracción, wc_agregado, wc_neteo, monto_cash_in, monto_cash_out) as
SELECT vw_transaction.transaction_date::date AS dia,
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok = 'WC_FOUND'::text THEN vw_transaction.amount
                         ELSE 0::numeric
                         END), 2)            AS "wc_extracción",
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok = 'WC CASH IN'::text THEN vw_transaction.amount
                         ELSE 0::numeric
                         END), 2)            AS wc_agregado,
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok = 'WORKER ACCOUNT BALANCER'::text THEN vw_transaction.amount
                         ELSE 0::numeric
                         END), 2)            AS wc_neteo,
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok = 'CASH IN CREDIT CARD'::text OR
                              vw_transaction.description_ok = 'CASH IN DEBIT CARD'::text OR
                              vw_transaction.description_ok = 'CASH IN CREDIT CARD'::text OR
                              vw_transaction.description_ok = 'EXTERNAL TRANSFER'::text THEN vw_transaction.amount
                         ELSE 0::numeric
                         END), 2)            AS monto_cash_in,
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok ~~ '%PAYMENT%'::text OR
                              vw_transaction.description_ok = 'CARD TRANSFER TO CBU'::text OR
                              vw_transaction.description_ok = 'TRANSFERS TO CBU'::text OR
                              vw_transaction.description_ok = 'BONIF/REINTEGROS'::text OR
                              vw_transaction.description_ok = 'PROMOTION CODE'::text THEN vw_transaction.amount
                         ELSE 0::numeric
                         END), 2)            AS monto_cash_out
FROM bi.vw_transaction
GROUP BY (vw_transaction.transaction_date::date);

alter table bi.vw_wc_movements
    owner to postgres;

grant delete, insert, references, select, trigger, truncate, update on bi.vw_wc_movements to bi;

--grant select on bi.vw_wc_movements to qs;

create view bi."vw_wc_movements_DELETE" (dia, wc_extracción, wc_agregado, wc_neteo, monto_cash_in, monto_cash_out) as
SELECT vw_transaction.transaction_date::date AS dia,
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok = 'WC_FOUND'::text THEN vw_transaction.amount
                         ELSE 0::numeric
                         END), 2)            AS "wc_extracción",
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok = 'WC CASH IN'::text THEN vw_transaction.amount
                         ELSE 0::numeric
                         END), 2)            AS wc_agregado,
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok = 'WORKER ACCOUNT BALANCER'::text THEN vw_transaction.amount
                         ELSE 0::numeric
                         END), 2)            AS wc_neteo,
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok = 'CASH IN CREDIT CARD'::text OR
                              vw_transaction.description_ok = 'CASH IN DEBIT CARD'::text OR
                              vw_transaction.description_ok = 'CASH IN CREDIT CARD'::text OR
                              vw_transaction.description_ok = 'EXTERNAL TRANSFER'::text THEN vw_transaction.amount
                         ELSE 0::numeric
                         END), 2)            AS monto_cash_in,
       round(sum(
                     CASE
                         WHEN vw_transaction.description_ok ~~ '%PAYMENT%'::text OR
                              vw_transaction.description_ok = 'CARD TRANSFER TO CBU'::text OR
                              vw_transaction.description_ok = 'TRANSFERS TO CBU'::text OR
                              vw_transaction.description_ok = 'BONIF/REINTEGROS'::text OR
                              vw_transaction.description_ok = 'PROMOTION CODE'::text THEN vw_transaction.amount
                         ELSE 0::numeric
                         END), 2)            AS monto_cash_out
FROM bi.vw_transaction
GROUP BY (vw_transaction.transaction_date::date);

alter table bi."vw_wc_movements_DELETE"
    owner to bi;

--grant select on bi."vw_wc_movements_DELETE" to qs;

create view bi.vw_wc_movements_dia(dia, fecha, wc_cashin, wc_fund, wc_total, wc_acumulado) as
WITH cte AS (
    SELECT '00 - Valor inicial'::text                         AS dia,
           '2020-01-01 00:00:00'::timestamp without time zone AS fecha,
           0                                                  AS wc_cashin,
           0                                                  AS wc_fund,
           '-1370737.43'::numeric + 5599.288 + 1642507.41     AS wc_total
    FROM ledger.transaction
    UNION
    SELECT to_char(vw_transaction.transaction_date, 'YYYY-MM-DD'::text) AS dia,
           max(vw_transaction.transaction_date)                         AS fecha,
           sum(
                   CASE
                       WHEN vw_transaction.origin_name = 'TAP BILLETERA SA'::text AND vw_transaction.amount > 0::numeric
                           THEN vw_transaction.amount
                       ELSE 0::numeric
                       END)                                             AS wc_cashin,
           sum(
                   CASE
                       WHEN vw_transaction.origin_name = 'TAP BILLETERA SA'::text AND vw_transaction.amount < 0::numeric
                           THEN vw_transaction.amount
                       ELSE 0::numeric
                       END)                                             AS wc_fund,
           sum(
                   CASE
                       WHEN vw_transaction.origin_name = 'TAP BILLETERA SA'::text THEN vw_transaction.amount
                       ELSE 0::numeric
                       END)                                             AS wc_total
    FROM bi.vw_transaction
    WHERE vw_transaction.origin_name = 'TAP BILLETERA SA'::text
    GROUP BY (to_char(vw_transaction.transaction_date, 'YYYY-MM-DD'::text))
),
     cteranked AS (
         SELECT cte.dia,
                max(cte.fecha)                       AS fecha,
                sum(cte.wc_cashin)                   AS wc_cashin,
                sum(cte.wc_fund)                     AS wc_fund,
                sum(cte.wc_total)                    AS wc_total,
                row_number() OVER (ORDER BY cte.dia) AS rownum
         FROM cte
         GROUP BY cte.dia, cte.fecha
     )
SELECT c1.dia,
       c1.fecha,
       c1.wc_cashin,
       c1.wc_fund,
       c1.wc_total,
       round((SELECT sum(c2.wc_total) AS sum
              FROM cteranked c2
              WHERE c2.rownum <= c1.rownum), 2) AS wc_acumulado
FROM cteranked c1;

alter table bi.vw_wc_movements_dia
    owner to bi;

--grant select on bi.vw_wc_movements_dia to qs;

create view bi."vw_wc_movements_diahora_DELETE"(dia_hora, wc_cashin, wc_fund, wc_total, wc_acumulado) as
WITH cte AS (
    SELECT '00 - Valor inicial'::text                     AS dia_hora,
           0                                              AS wc_cashin,
           0                                              AS wc_fund,
           '-1370737.43'::numeric + 5599.288 + 1642507.41 AS wc_total
    FROM ledger.transaction
    UNION
    SELECT to_char(vw_transaction.transaction_date, 'YYYY-MM-DD HH24:MI'::text) AS dia_hora,
           round(sum(
                         CASE
                             WHEN vw_transaction.origin_name = 'TAP BILLETERA SA'::text AND vw_transaction.amount > 0::numeric
                                 THEN vw_transaction.amount
                             ELSE 0::numeric
                             END), 2)                                           AS wc_cashin,
           round(sum(
                         CASE
                             WHEN vw_transaction.origin_name = 'TAP BILLETERA SA'::text AND vw_transaction.amount < 0::numeric
                                 THEN vw_transaction.amount
                             ELSE 0::numeric
                             END), 2)                                           AS wc_fund,
           round(sum(
                         CASE
                             WHEN vw_transaction.origin_name = 'TAP BILLETERA SA'::text THEN vw_transaction.amount
                             ELSE 0::numeric
                             END), 2)                                           AS wc_total
    FROM bi.vw_transaction
    WHERE vw_transaction.origin_name = 'TAP BILLETERA SA'::text
    GROUP BY vw_transaction.transaction_date, (to_char(vw_transaction.transaction_date, 'DD/MM HH24:MI'::text))
),
     cteranked AS (
         SELECT cte.dia_hora,
                sum(cte.wc_cashin)                        AS wc_cashin,
                sum(cte.wc_fund)                          AS wc_fund,
                sum(cte.wc_total)                         AS wc_total,
                row_number() OVER (ORDER BY cte.dia_hora) AS rownum
         FROM cte
         GROUP BY cte.dia_hora
     )
SELECT c1.dia_hora,
       c1.wc_cashin,
       c1.wc_fund,
       c1.wc_total,
       (SELECT sum(c2.wc_total) AS sum
        FROM cteranked c2
        WHERE c2.rownum <= c1.rownum) AS wc_acumulado
FROM cteranked c1
ORDER BY c1.dia_hora;

alter table bi."vw_wc_movements_diahora_DELETE"
    owner to bi;

--grant select on bi."vw_wc_movements_diahora_DELETE" to qs;

create view bi."vw_wc_movements_historico_DELETE"(dia_hora, wc_cashin, wc_found, wc_balancer, wc_acumulado) as
WITH cte AS (
    SELECT '00 - Valor inicial'::text AS dia_hora,
           round(sum(
                         CASE
                             WHEN vw_transaction.description_ok = 'WC_FOUND'::text THEN vw_transaction.amount
                             ELSE 0::numeric
                             END), 2) AS wc_found,
           round(sum(
                         CASE
                             WHEN vw_transaction.description_ok = 'WC CASH IN'::text THEN vw_transaction.amount
                             ELSE 0::numeric
                             END), 2) AS wc_cashin,
           round(sum(
                         CASE
                             WHEN vw_transaction.description_ok = 'WORKER ACCOUNT BALANCER'::text
                                 THEN vw_transaction.amount
                             ELSE 0::numeric
                             END), 2) AS wc_balancer,
           round(sum(
                         CASE
                             WHEN vw_transaction.description_ok = 'WORKER ACCOUNT BALANCER'::text
                                 THEN vw_transaction.amount
                             ELSE 0::numeric
                             END) + sum(
                         CASE
                             WHEN vw_transaction.description_ok = 'WC_FOUND'::text THEN vw_transaction.amount
                             ELSE 0::numeric
                             END) + sum(
                         CASE
                             WHEN vw_transaction.description_ok = 'WC CASH IN'::text THEN vw_transaction.amount
                             ELSE 0::numeric
                             END), 2) AS wc_final
    FROM bi.vw_transaction
    WHERE vw_transaction.transaction_date < '2020-11-28 00:00:00'::timestamp without time zone
      AND (vw_transaction.description_ok = 'WC CASH IN'::text OR vw_transaction.description_ok = 'WC_FOUND'::text OR
           vw_transaction.description_ok = 'WORKER ACCOUNT BALANCER'::text)
    UNION
    SELECT to_char(vw_transaction.transaction_date, 'DD/MM HH24:MI:SS'::text) AS dia_hora,
           round(sum(
                         CASE
                             WHEN vw_transaction.description_ok = 'WC_FOUND'::text THEN vw_transaction.amount
                             ELSE 0::numeric
                             END), 2)                                         AS wc_found,
           round(sum(
                         CASE
                             WHEN vw_transaction.description_ok = 'WC CASH IN'::text THEN vw_transaction.amount
                             ELSE 0::numeric
                             END), 2)                                         AS wc_cashin,
           round(sum(
                         CASE
                             WHEN vw_transaction.description_ok = 'WORKER ACCOUNT BALANCER'::text
                                 THEN vw_transaction.amount
                             ELSE 0::numeric
                             END), 2)                                         AS wc_balancer,
           round(sum(
                         CASE
                             WHEN vw_transaction.description_ok = 'WORKER ACCOUNT BALANCER'::text
                                 THEN vw_transaction.amount
                             ELSE 0::numeric
                             END) + sum(
                         CASE
                             WHEN vw_transaction.description_ok = 'WC_FOUND'::text THEN vw_transaction.amount
                             ELSE 0::numeric
                             END) + sum(
                         CASE
                             WHEN vw_transaction.description_ok = 'WC CASH IN'::text THEN vw_transaction.amount
                             ELSE 0::numeric
                             END), 2)                                         AS wc_final
    FROM bi.vw_transaction
    WHERE vw_transaction.transaction_date >= '2020-11-28 00:00:00'::timestamp without time zone
      AND (vw_transaction.description_ok = 'WC CASH IN'::text OR vw_transaction.description_ok = 'WC_FOUND'::text OR
           vw_transaction.description_ok = 'WORKER ACCOUNT BALANCER'::text)
    GROUP BY (to_char(vw_transaction.transaction_date, 'DD/MM HH24:MI:SS'::text))
),
     cteranked AS (
         SELECT cte.wc_final,
                cte.dia_hora,
                cte.wc_found,
                cte.wc_cashin,
                cte.wc_balancer,
                row_number() OVER (ORDER BY cte.dia_hora) AS rownum
         FROM cte
     )
SELECT c1.dia_hora,
       c1.wc_cashin,
       c1.wc_found,
       c1.wc_balancer,
       (SELECT sum(c2.wc_final) AS sum
        FROM cteranked c2
        WHERE c2.rownum <= c1.rownum) AS wc_acumulado
FROM cteranked c1;

alter table bi."vw_wc_movements_historico_DELETE"
    owner to bi;

--grant select on bi."vw_wc_movements_historico_DELETE" to qs;

create view bi.vw_wc_movements_moveid(dia_hora, movement_id, wc_cashin, wc_fund, wc_total, wc_acumulado) as
WITH cte AS (
    SELECT '00 - Valor inicial'::text                     AS dia_hora,
           0                                              AS movement_id,
           0                                              AS wc_cashin,
           0                                              AS wc_fund,
           '-1370737.43'::numeric + 5599.288 + 1642507.41 AS wc_total
    FROM ledger.transaction
    UNION
    SELECT to_char(vw_transaction.transaction_date, 'YYYY-MM-DD HH24:MI'::text) AS dia_hora,
           vw_transaction.movement_id,
           sum(
                   CASE
                       WHEN vw_transaction.origin_name = 'TAP BILLETERA SA'::text AND vw_transaction.amount > 0::numeric
                           THEN vw_transaction.amount
                       ELSE 0::numeric
                       END)                                                     AS wc_cashin,
           sum(
                   CASE
                       WHEN vw_transaction.origin_name = 'TAP BILLETERA SA'::text AND vw_transaction.amount < 0::numeric
                           THEN vw_transaction.amount
                       ELSE 0::numeric
                       END)                                                     AS wc_fund,
           sum(
                   CASE
                       WHEN vw_transaction.origin_name = 'TAP BILLETERA SA'::text THEN vw_transaction.amount
                       ELSE 0::numeric
                       END)                                                     AS wc_total
    FROM bi.vw_transaction
    WHERE vw_transaction.origin_name = 'TAP BILLETERA SA'::text
    GROUP BY vw_transaction.transaction_date, vw_transaction.movement_id
),
     cteranked AS (
         SELECT cte.dia_hora,
                cte.movement_id,
                sum(cte.wc_cashin)                           AS wc_cashin,
                sum(cte.wc_fund)                             AS wc_fund,
                sum(cte.wc_total)                            AS wc_total,
                row_number() OVER (ORDER BY cte.movement_id) AS rownum
         FROM cte
         GROUP BY cte.dia_hora, cte.movement_id
     )
SELECT c1.dia_hora,
       c1.movement_id,
       c1.wc_cashin,
       c1.wc_fund,
       c1.wc_total,
       (SELECT round(sum(c2.wc_total), 2) AS round
        FROM cteranked c2
        WHERE c2.rownum <= c1.rownum) AS wc_acumulado
FROM cteranked c1;

alter table bi.vw_wc_movements_moveid
    owner to bi;

--grant select on bi.vw_wc_movements_moveid to qs;

